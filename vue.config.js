const path = require('path')

module.exports = {
  // 為了讓包完以後的 html 讀得到靜態檔
  publicPath: './',
  configureWebpack: {
    resolve: {
      alias: {
        // 在 .js/.vue 裡 import 不必再用相對路徑
        src: path.resolve(__dirname, 'src')
      }
    },
  },
  // exclude folders from webpack bundle
  chainWebpack: (config) => {
    config.plugin("copy").tap(([options]) => {
      options[0].ignore.push("jsondata/**");
      return [options];
    });
  }
}
