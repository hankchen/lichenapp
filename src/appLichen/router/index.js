// vue basic lib
import Vue from 'vue'
import VueRouter from 'vue-router'
Vue.use(VueRouter)

/* 同路由不跳紅字 */	// NavigationDuplicated is not allowed
// const routerPush = VueRouter.prototype.push
// // VueRouter.prototype.replace = 
// VueRouter.prototype.push = function(location) {
	// // console.log('VueRouter-location: ', location);	// @@
  // return routerPush.call(this, location)
	// // .catch(error=> error)
	// .catch(error => {
		// console.error('同路由不跳紅字: ', error);	// NG,券popup時仍無效
	// })
// };


// vue router 設定(桌面版)
const routes = [
  // === 卡卡主頁面 ===
  { path: '/',      component: () => import('src/appLichen/pages/Index.vue') },
  { path: '/login', component: () => import('src/appLichen/pages/Login.vue') },
  { path: '/setting', component: () => import('src/appLichen/pages/Setting.vue') },
  { path: '/register', component: () => import('src/appLichen/pages/Register.vue') },
  { path: '/404' },
  // === 企業 APP 系列 ===
  // 企業首頁
  { path: '/card', meta: { layout: 'Card' }, component: () => import('src/appLichen/pages/cards/Index.vue') },
  // 會員頁
  { path: '/cardMember', meta: { layout: 'Card' }, component: () => import('src/appLichen/pages/cards/Member.vue') },
  // 積點紀錄
  { path: '/cardPoint', meta: { layout: 'Card' }, component: () => import('src/appLichen/pages/cards/Point.vue') },
  // 點數贈點
  { path: '/pointGive', meta: { layout: 'Card' }, component: () => import('src/appLichen/pages/cards/PointGive.vue') },
  // 消費紀錄
  { path: '/cardPay', meta: { layout: 'Card' }, component: () => import('src/appLichen/pages/cards/Pay.vue') },
  // 儲值紀錄
  { path: '/cardPrepay', meta: { layout: 'Card' }, component: () => import('src/appLichen/pages/cards/Prepay.vue') },
  // 優惠卷
  { path: '/cardCoupon', meta: { layout: 'Card' }, component: () => import('src/appLichen/pages/cards/Coupon.vue') },
  // 會員資訊
  { path: '/cardProfile', meta: { layout: 'Card' }, component: () => import('src/appLichen/pages/cards/Profile.vue') },
  // // 門市查詢
  { path: '/cardStore', meta: { layout: 'Card' }, component: () => import('src/appLichen/pages/cards/Store.vue') },
  // 產品列表頁
  { path: '/cardMenu/:id', meta: { layout: 'Card' }, component: () => import('src/appLichen/pages/cards/Menu.vue') },
  // // 產品介紹頁
  { path: '/cardProducts/:id', meta: { layout: 'Card' }, component: () => import('src/appLichen/pages/cards/Products.vue') },
  // 消息列表頁
  { path: '/cardNews', meta: { layout: 'Card' }, component: () => import('src/appLichen/pages/cards/News.vue') },
  // 消息介紹頁
  { path: '/cardNewspage/:id', meta: { layout: 'Card' }, component: () => import('src/appLichen/pages/cards/Newspage.vue') },
  // 品牌專區
  { path: '/cardBrand', meta: { layout: 'Card' }, component: () => import('src/appLichen/pages/cards/Brand.vue') },
  // 常見問題頁
  { path: '/cardFaq', meta: { layout: 'Card' }, component: () => import('src/appLichen/pages/cards/Faq.vue') },
  // 商品禮卷(寄杯)頁
  { path: '/cardGiftVouchers', meta: { layout: 'Card' }, component: () => import('src/appLichen/pages/cards/GiftVouchers.vue') },
  // 集點換券頁
  { path: '/pointExg', meta: { layout: 'Card' }, component: () => import('src/appLichen/pages/cards/Exchange.vue') },
  // 限時搶券
  { path: '/getCoupon', meta: { layout: 'Card' }, component: () => import('src/appLichen/pages/cards/GetCoupon.vue') },
  // 享聚卡回娘家
  { path: '/oldCard', meta: { layout: 'Card' }, component: () => import('src/appLichen/pages/cards/OldCard.vue') },
  // 點數轉贈
  { path: '/pointGive', meta: { layout: 'Card' }, component: () => import('src/appLichen/pages/cards/PointGive.vue') },
]

export default new VueRouter({ routes })
