/* 商城共用應用 */
import store from 'src/store'
import { showToast,showAlert } from 'src/libs/appHelper.js'
import fetchData from 'src/libs/fetchData.js'
// import { malltmp } from 'src/assets/mall_temp.js'
//import router from 'src/router'
//import {deviseFunction} from 'src/libs/deviseHelper.js'
var isFn = isFn || function(f) {
    return typeof f === 'function';
}

function changeCart(p_obj,isIncNum,userCount,isDel,goCartfn,delFn){
    //購物車異動  
    //const findFood = (et) => et.FoodID == p_obj.FoodID;
    // const findFood = (et) => et.styleFoodID == p_obj.styleFoodID; //依規格的ID來區分群組
    const findFood = (et) => et.FoodID == p_obj.FoodID; //依規格的ID來區分群組
    
    var cart_index = store.state.mallData.shopping_cart.findIndex(findFood);
    if (isDel && cart_index > -1){
        //刪單筆                
        // showToast('購物車已變更');
        deleteCart(p_obj,(all_cart)=>{
            // store.state.mallData.shopping_cart = Object.assign([], all_cart); 
            store.commit('setShopCart', Object.assign([], all_cart));
            itmeQtyReload();
            if (isFn(delFn)) delFn();
        });
        return;
    }
    var delIndex = -1;
    store.state.mallData.foodQty[p_obj.MainID] = store.state.mallData.foodQty[p_obj.MainID] || 0;
    store.state.mallData.kindQty[p_obj.KindID] = store.state.mallData.kindQty[p_obj.KindID] || 0;

    //console.log(store.state.mallData);
    if (store.state.mallData.shopping_cart.length == 0 && isIncNum){
        newCart(p_obj,userCount,goCartfn); 
        // showToast('購物車已變更');

    }else{
        if (cart_index >= 0){
            //只變動數量
            
            var cart_obj = Object.assign({}, store.state.mallData.shopping_cart[cart_index]);
            cart_obj.Count = toDecimal(cart_obj.Count);
            
            cart_obj.Price = toDecimal(cart_obj.Price);
            if (isIncNum) {
                if (chkTypeInt(cart_obj.BatchID) != 0){
                    if (userCount != undefined){
                        cart_obj.Count += userCount; 
                    }else{
                        cart_obj.Count++;                    
                        //console.log("qqqqq>>>>",cart_obj.Total,cart_obj.Total,isIncNum);
                    }
                }else{
                    //特殊商品只能新增cart item
                    cart_obj.Count=0;
                    newCart(p_obj,userCount,goCartfn);
                }
                


            }else if (!isIncNum && cart_obj.Count > 0) {
                cart_obj.Count--; 
                if (cart_obj.Count == 0){
                    delIndex = cart_index;
                }

            }
            cart_obj.Total = toDecimal(cart_obj.Price * cart_obj.Count);     
            
            if (cart_obj.Count > 0) sendUpdateCart(cart_obj,(all_cart)=>{
                if (isFn(goCartfn)){
                    //router跳轉時,會load shop cart
                    goCartfn();
                }else{
                    if (Array.isArray(all_cart)) {
                        store.state.mallData.shopping_cart = Object.assign([], all_cart); 
                        itmeQtyReload();
                    }
                    
                    
                }
                
            })
            //showToast('購物車已變更');
            
        }else if (isIncNum){
            //找不到item,又是按增加+時
            newCart(p_obj,userCount,goCartfn);        
            
            //showToast('購物車已變更');
        }

    }

    if (delIndex > -1){
        deleteCart(p_obj,(all_cart)=>{
            store.state.mallData.shopping_cart = Object.assign([], all_cart);
            if (isFn(delFn)) delFn();
            itmeQtyReload();
        });
        // store.state.mallData.shopping_cart.splice(delIndex, 1);
    }
    store.state.mallData.isCarUI = !store.state.mallData.isCarUI;
    //console.log("ca ..>>",store.state.mallData.shopping_cart);
}
function itmeQtyReload(){
    //重算購物車上,品項及類別數量
    store.state.mallData.foodQty = {};
    store.state.mallData.kindQty = {};
    store.state.mallData.shopping_cart.map((item)=>{
        var p_Qty = toDecimal(item.Count);
        store.state.mallData.foodQty[item.MainID] = store.state.mallData.foodQty[item.MainID] || 0;
        store.state.mallData.kindQty[item.KindID] = store.state.mallData.kindQty[item.KindID] || 0;
        store.state.mallData.foodQty[item.MainID] += p_Qty;
        store.state.mallData.kindQty[item.KindID] += p_Qty;

    });
    cartQty(store.state.mallData.kindQty)
    store.state.mallData.isCarUI = !store.state.mallData.isCarUI;
}
async function sendNewCart(p_obj,fn){
    //新增購物車
    store.state.isLoading = true;
    const url =  store.getters.appBookUrlWuhu;
    const body = store.getters.appBodyWuhu(Object.assign({}, {"act": 'add_newshopcart'}, p_obj));
    
    const data = await fetchData({url, body});
    var isErr = data.detail && data.detail.ErrorCode && data.detail.ErrorCode != "0";  
    store.state.isLoading = false;
    if (!isErr && !data.error){      
        if (data.ErrorMsg) showAlert(data.ErrorMsg);              
        showToast('購物車已變更');        
        if (Array.isArray(data.ItemsTemp) && data.ItemsTemp.length > 0) {if (isFn(fn)) fn(data.ItemsTemp[0]);}
    }else{ 
        //{"ErrorCode":"1","ErrorMsg":"要求已經中止: 無法建立 SSL/TLS 的安全通道。","Remark":""}   
        // {"ErrorCode":"3","ErrorMsg":"商品庫存不足或己下架","Remark":null} 
        if (data.detail && data.detail.ErrorCode === '3'){
            showToast(data.detail.ErrorMsg);
        }else{
            msgShort(data.detail.ErrorMsg);
        }
        if (isFn(fn)) fn();
        
    }
    
}
async function sendUpdateCart(p_obj,fn){    
    //更新購物車
    store.state.isLoading = true;
    const url =  store.getters.appBookUrlWuhu;
    const body = store.getters.appBodyWuhu(Object.assign({}, {"act": 'update_shopitem'}, p_obj));
    
    const data = await fetchData({url, body});
    var isErr = data.detail && data.detail.ErrorCode && data.detail.ErrorCode != "0"; 
    store.state.isLoading = false;    
    //P_ItemsTemp_Web, ItemsTemp 有二組?
    //變更數量沒有作用?    
    if (!isErr && !data.error){   
        if (data.ErrorMsg) showAlert(data.ErrorMsg);                 
        showToast('購物車已變更');        
        if(Array.isArray(data.ItemsTemp)){if (isFn(fn)) fn(data.ItemsTemp);}
        if(Array.isArray(data.OrdersTemp) && data.OrdersTemp.length > 0) store.state.mallData.shopOrderTmp = Object.assign({}, data.OrdersTemp[0]);
    }else{        
        if (data.detail && data.detail.ErrorCode === '3'){
            showToast(data.detail.ErrorMsg);
        }else{
            msgShort(data.detail.ErrorMsg);
        }
        if (isFn(fn)) fn();
    }
    
}


async function getDeedpoints(fn){
    // 取消費點數
    store.state.isLoading = true;
    const url =  store.getters.appBookUrlWuhu;
    const body = store.getters.appBodyWuhu({"act": 'get_deedpoints'});
    
    const data = await fetchData({url, body});
    var isErr = data.detail && data.detail.ErrorCode && data.detail.ErrorCode != "0"; 
    store.state.isLoading = false;    
    if (!isErr && !data.error){ 
        if (data.ErrorMsg) showAlert(data.ErrorMsg);
        if (isFn(fn)) fn(data);
    }else{
        msgShort(data.detail.ErrorMsg);
        if (isFn(fn)) fn([]);
    }
    
}
async function getMyShopCart(fn,isShowMsg){
    // 取購物車
    store.state.isLoading = true;
    const url =  store.getters.appBookUrlWuhu;
    const body = store.getters.appBodyWuhu({"act": 'get_myshopcart'});
    
    const data = await fetchData({url, body});
    var isErr = data.detail && data.detail.ErrorCode && data.detail.ErrorCode != "0"; 
    store.state.isLoading = false;    
    if (!isErr && !data.error){ 
        //ErrorMsg
        if (data.ErrorMsg && isShowMsg) showAlert(data.ErrorMsg);
        if(Array.isArray(data.ItemsTemp)) store.state.mallData.shopping_cart = Object.assign([], data.ItemsTemp); 
        if(Array.isArray(data.OrdersTemp) && data.OrdersTemp.length > 0) store.state.mallData.shopOrderTmp = Object.assign({}, data.OrdersTemp[0]);  
        if(Array.isArray(data.FoodMarket)) foodCheck(data.FoodMarket,true);
        
        if (isFn(fn)) fn(data);
    }else{
        msgShort(data.detail.ErrorMsg);
    }
    
}
async function getOrderStatus(fn){
    // 訂單狀態
    if (Object.keys(store.state.mallData.orderstatus).length > 0){if (isFn(fn)) fn();return;}
    store.state.isLoading = true;
    const url =  store.getters.appBookUrlWuhu;
    const body = store.getters.appBodyWuhu({"act": 'get_orderstatus'});
    
    const data = await fetchData({url, body});
    var isErr = data.detail && data.detail.ErrorCode && data.detail.ErrorCode != "0";  
    store.state.isLoading = false;    
    if (!isErr && !data.error){  
        //\"Code\":\"0\",\"Text\":\"待接單\"
        if (data.ErrorMsg) showAlert(data.ErrorMsg);
        if(Array.isArray(data.orderstatus)){
            data.orderstatus.map((p_status)=>{
                store.state.mallData.orderstatus[p_status.Code] = p_status.Text;
            });
            
        }                  
        if (isFn(fn)) fn();
    }else{
        msgShort(data.detail.ErrorMsg);
    }
    
}
async function cleanCart(fn){
    //清空購物車
    store.state.isLoading = true;
    const url =  store.getters.appBookUrlWuhu;
    const body = store.getters.appBodyWuhu({"act": 'clean_myshopcart'});
    
    const data = await fetchData({url, body});
    var isErr = data.ErrorCode && data.ErrorCode != "0";  
    store.state.isLoading = false;
    if (!isErr && !data.error){
        if (data.ErrorMsg) {showAlert(data.ErrorMsg);} else {showToast('購物車已變更');}
        
        if (isFn(fn)) fn();                       
        
    }else{
        msgShort(data.detail.ErrorMsg);
    }
    
}
async function deleteCart(p_obj,fn){
    //刪除購物車品項    
    store.state.isLoading = true;
    const url =  store.getters.appBookUrlWuhu;
    
    const body = store.getters.appBodyWuhu({"act": 'delete_shopitem',"ID":p_obj.ID});
    
    const data = await fetchData({url, body});
    var isErr = data.detail && data.detail.ErrorCode && data.detail.ErrorCode != "0"; 
    store.state.isLoading = false;
    if (!isErr && !data.error){
        if (data.ErrorMsg) {showAlert(data.ErrorMsg);} else {showToast('購物車已變更');}       
        if(Array.isArray(data.ItemsTemp)){if (isFn(fn)) fn(data.ItemsTemp);}                    
        if(Array.isArray(data.OrdersTemp) && data.OrdersTemp.length > 0) store.state.mallData.shopOrderTmp = Object.assign({}, data.OrdersTemp[0]);
    }else{
        msgShort(data.detail.ErrorMsg);
    }
    
}
async function goCheckout(IDs,deedNo,vipInfo, trickIDs, fn){
    //結帳 (自選付款)
    trickIDs = trickIDs || [];
    store.state.isLoading = true;
    const url =  store.getters.appBookUrlWuhu; 
    const body = store.getters.appBodyWuhu(Object.assign({}, {"act": 'new_checkout',"ID":IDs,"deedPoint":deedNo,"TicketInfoID":trickIDs}, vipInfo));
    const data = await fetchData({url, body});
    var isErr = data.detail && data.detail.ErrorCode && data.detail.ErrorCode != "0"; 
    store.state.isLoading = false;
    if (!isErr && !data.error){
        if (data.ErrorMsg) showAlert(data.ErrorMsg);
        if (isFn(fn)) fn(data);
    //    console.log("data>>>",data);
    }else{
        msgShort(data.detail.ErrorMsg);
    }
    
}
async function goCalcuCheckout(IDs,trickIDs,fn){
    //結帳 (驗算付款)  d
    trickIDs = trickIDs || [];
    store.state.isLoading = true;
    const url =  store.getters.appBookUrlWuhu;
    const body = store.getters.appBodyWuhu({"act": 'calc_checkout',"ID":IDs,"TicketInfoID":trickIDs});
    
    const data = await fetchData({url, body});
    var isErr = data.detail && data.detail.ErrorCode && data.detail.ErrorCode != "0"; 
    store.state.isLoading = false;
    if (!isErr && !data.error){
        if (data.ErrorMsg) showAlert(data.ErrorMsg);
        if(Array.isArray(data.FoodMarket)) foodCheck(data.FoodMarket,true);
        if (isFn(fn)) fn(data);
    //    console.log("goCalcuCheckout..data>>>",data);
    }else{
        msgShort(data.detail.ErrorMsg);
    }
    
}
async function acceptCoupon(IDs,trickIDs,fn){
    //結帳 (驗算付款)  
    trickIDs = trickIDs ||[];
    store.state.isLoading = true;
    const url =  store.getters.appBookUrlWuhu;    
    const body = store.getters.appBodyWuhu({"act": 'accept_coupon',"ID":IDs,"TicketInfoID":trickIDs});
    
    const data = await fetchData({url, body});
    var isErr = data.detail && data.detail.ErrorCode && data.detail.ErrorCode != "0"; 
    store.state.isLoading = false;
    if (!isErr && !data.error){
        if (data.ErrorMsg) showAlert(data.ErrorMsg);
        if (isFn(fn)) fn(data);
    //    console.log("goCalcuCheckout..data>>>",data);
    }else{
        msgShort(data.detail.ErrorMsg);
    }
    
}
async function checkCoupon(IDs,trickIDs,fn){
    //結帳 (驗算付款)  
    trickIDs = trickIDs ||[];
    store.state.isLoading = true;
    const url =  store.getters.appBookUrlWuhu;    
    const body = store.getters.appBodyWuhu({"act": 'check_coupon',"ID":IDs,"TicketInfoID":trickIDs});
    
    const data = await fetchData({url, body});
    var isErr = data.detail && data.detail.ErrorCode && data.detail.ErrorCode != "0"; 
    store.state.isLoading = false;
    if (!isErr && !data.error){
        if (data.ErrorMsg) showAlert(data.ErrorMsg);
        if (isFn(fn)) fn(data);
    //    console.log("goCalcuCheckout..data>>>",data);
    }else{
        msgShort(data.detail.ErrorMsg);
    }
    
}

async function finishCheckout(orderID,deedNo,vipInfo,trickIDs,fn){
    //結帳 (自選付款)  
    trickIDs = trickIDs || [];
    store.state.isLoading = true;
    const url =  store.getters.appBookUrlWuhu;    
    const body = store.getters.appBodyWuhu(Object.assign({}, {"act": 'finish_checkout',"ID":orderID,"deedPoint":deedNo,"TicketInfoID":trickIDs}, vipInfo));
    const data = await fetchData({url, body});
    var isErr = data.detail && data.detail.ErrorCode && data.detail.ErrorCode != "0"; 
    store.state.isLoading = false;
    if (!isErr && !data.error){
        if (data.ErrorMsg) showAlert(data.ErrorMsg);
        if (isFn(fn)) fn(data);
        
    }else{
        msgShort(data.detail.ErrorMsg);
    }
    
}

async function quickCheckout(IDs,vipInfo,trickIDs,fn){
    //quick_checkout 快速結帳 (快速付款)  
    store.state.isLoading = true;
    trickIDs = trickIDs || [];
    const url =  store.getters.appBookUrlWuhu;    
    // const body = store.getters.appBodyWuhu({"act": 'quick_checkout',"ID":IDs});
    const body = store.getters.appBodyWuhu(Object.assign({}, {"act": 'quick_checkout',"ID":IDs,"TicketInfoID":trickIDs}, vipInfo));
    const data = await fetchData({url, body});
    
    var isErr = data && data.detail && data.detail.ErrorCode && data.detail.ErrorCode != "0"; 
    store.state.isLoading = false;
    
    if (data && !isErr && !data.error){
        if (data.ErrorMsg) showAlert(data.ErrorMsg);
        if (isFn(fn)) fn(data);
    //    console.log("data>>>",data);
    }else{
        msgShort(data.detail.ErrorMsg);
    }
    
}
async function singlefoodmarket(p_id,fn){
    //取單一品項 
    store.state.isLoading = true;
    const url =  store.getters.appBookUrlWuhu;    
    const body = store.getters.appBodyWuhu({"act": 'get_singlefoodmarket',"MainID":p_id});
    
    const data = await fetchData({url, body});
    
    var isErr = data && data.detail && data.detail.ErrorCode && data.detail.ErrorCode != "0"; 
    store.state.isLoading = false;
    
    if (data && !isErr && !data.error){
        if (data.ErrorMsg) showAlert(data.ErrorMsg);
        if (isFn(fn)) fn(foodCheck(data.FoodMarket,true));
    //    console.log("data>>>",data);
    }else{
        if (isFn(fn)) fn([]);
        msgShort(data.detail.ErrorMsg);
    }
    
}
async function searchFood(keyFilter,fn){
    //搜尋品項名稱
    store.state.isLoading = true;
    const url =  store.getters.appBookUrlWuhu;    
    const body = store.getters.appBodyWuhu({"act": 'search_foodmarket',"FoodName":keyFilter});
    
    const data = await fetchData({url, body});
    
    var isErr = data && data.detail && data.detail.ErrorCode && data.detail.ErrorCode != "0"; 
    store.state.isLoading = false;
    
    if (data && !isErr && !data.error){
        if (data.ErrorMsg) showAlert(data.ErrorMsg);
        if (isFn(fn)) fn(foodCheck(data.FoodMarket,true));
    //    console.log("data>>>",data);
    }else{
        msgShort(data.detail.ErrorMsg);
    }
    
}
async function getExpired(fn){
    //體驗點數
    store.state.isLoading = true;
    const url =  store.getters.appBookUrlWuhu;    
    const body = store.getters.appBodyWuhu({"act": 'GetVipAndCardInfo'});
    
    const data = await fetchData({url, body});
    
    var isErr = data && data.detail && data.detail.ErrorCode && data.detail.ErrorCode != "0"; 
    store.state.isLoading = false;
      
    if (data && !isErr && !data.error){
        if (data.ErrorMsg) showAlert(data.ErrorMsg);
        if (isFn(fn)) fn(data);
    //    console.log("data>>>",data);
    }else{
        msgShort(data.detail.ErrorMsg);
    }
    
}
async function getMyorders(fn){
    //取得訂單
    store.state.isLoading = true;
    const url =  store.getters.appBookUrlWuhu;    
    const body = store.getters.appBodyWuhu({"act": 'get_myorders'});
    
    const data = await fetchData({url, body});
    var isErr = data.detail && data.detail.ErrorCode && data.detail.ErrorCode != "0"; 
    store.state.isLoading = false;  
    // store.state.mallData.orders=[];
    store.commit('setShopRecord', []);
    if (!isErr && !data.error){
        if (data.ErrorMsg) showAlert(data.ErrorMsg);
        if (Array.isArray(data.Orders)) {
            // store.state.mallData.orders = data.Orders;
            store.commit('setShopRecord', data.Orders);
            // getOrderitems(store.state.mallData.orders[0]["ID"],0,fn);

        }
        
    }else{
        msgShort(data.detail.ErrorMsg);
    }
    if (isFn(fn)) fn();
}
async function getOrderitems(orderID,fn){
    //取得訂單品項 
    store.state.isLoading = true;
    const url =  store.getters.appBookUrlWuhu;    
    const body = store.getters.appBodyWuhu({"act": 'get_orderitems',"OrderID":orderID});
    
    const data = await fetchData({url, body});
    var isErr = data.detail && data.detail.ErrorCode && data.detail.ErrorCode != "0"; 
    store.state.isLoading = false;
    if (!isErr && !data.error){
        if (data.ErrorMsg) showAlert(data.ErrorMsg);
        if (isFn(fn)) fn(data.OrderItem,data.Checks,data.Agios);
       
    }else{
        msgShort(data.detail.ErrorMsg);
    }

}
// async function getOrderitems(orderID,orIndex,fn){
//     //取得訂單品項 
//     //store.state.isLoading = true;
//     const url =  store.getters.appBookUrlWuhu;    
//     const body = store.getters.appBodyWuhu({"act": 'get_orderitems',"OrderID":orderID});
    
//     const data = await fetchData({url, body});
//     var isErr = data.detail && data.detail.ErrorCode && data.detail.ErrorCode != "0"; 
//     //store.state.isLoading = false;
//     if (!isErr && !data.error){
//         store.state.mallData.orders[orIndex]["items"] = store.state.mallData.orders[orIndex]["items"] || [];
//         if (Array.isArray(data.OrderItem)) {
//             store.state.mallData.orders[orIndex]["items"] = data.OrderItem || [];
//             //console.log("data>>>>>>",store.state.mallData.orders[orIndex]["items"]);
//         }
       
//     }else{
//         //msgShort(data.detail.ErrorMsg);
//     }
//     const next_index = orIndex+1;
//     if (store.state.mallData.orders.length > next_index){
//         getOrderitems(store.state.mallData.orders[next_index]["ID"],next_index,fn);
//     }else{
        
//         if (isFn(fn)) fn();
//     }
// }
async function cancelMyorder(orderID,fn){
    //取消訂單
    store.state.isLoading = true;
    const url =  store.getters.appBookUrlWuhu;    
    const body = store.getters.appBodyWuhu({"act": 'cancel_myorder',"OrderID":orderID});
    
    const data = await fetchData({url, body});
    var isErr = data.detail && data.detail.ErrorCode && data.detail.ErrorCode != "0"; 
    store.state.isLoading = false;
    
    if (!isErr && !data.error){
        if (data.ErrorMsg){
            showAlert(data.ErrorMsg);
        }else if (Array.isArray(data.Orders)) {            
            store.commit('setShopRecord', Object.assign([], data.Orders)); 
            // store.state.mallData.orders = data.Orders;
            // getOrderitems(store.state.mallData.orders[0]["ID"],0,fn);

        }
    //    console.log("data>>>",data);
        
    }else{ 
                    
        msgShort(data.detail.ErrorMsg); 
        
    }
    if (isFn(fn)) fn(data);
}
async function getUnfinishorder(fn) {      
    //檢視未付款的訂單,另外需要取DeedPoints
    store.state.isLoading = true;
    const url =  store.getters.appBookUrlWuhu;    
    const body = store.getters.appBodyWuhu({"act": 'get_unfinishorder'});
    const data = await fetchData({url, body});
    var isErr = data.detail && data.detail.ErrorCode && data.detail.ErrorCode != "0"; 
    store.state.isLoading = false;
    if (!isErr && !data.error){
        if (data.ErrorMsg) showAlert(data.ErrorMsg);
        // if (isFn(fn)) fn(data.Orders); 
        if (isFn(fn)) fn(data.OrdersTemp); 
        // if (Array.isArray(data.Orders) && data.Orders.length > 0) getUnfinishorderItem(data.Orders,0,fn);
        
    }else{
        msgShort(data.detail.ErrorMsg);
    }
    
    
}
async function getUnfinishorderItem(orderID,fn){
    //取得訂單品項 
    store.state.isLoading = true;
    if (!orderID) if (isFn(fn)) {fn();}else{return;}
    const url =  store.getters.appBookUrlWuhu;    
    const body = store.getters.appBodyWuhu({"act": 'get_unfinishorder_item',"OrderID":orderID});
    
    const data = await fetchData({url, body});
    
    var isErr = data.detail && data.detail.ErrorCode && data.detail.ErrorCode != "0"; 
    store.state.isLoading = false;
    if (!isErr && !data.error){
        if (data.ErrorMsg) showAlert(data.ErrorMsg);
        if (isFn(fn)) fn(data.OrderItem);
        
    }else{
        msgShort(data.detail.ErrorMsg);
    }
    
    
}
// async function getUnfinishorderItem(orders,orIndex,fn){
//     //取得訂單品項 
//     //store.state.isLoading = true;
//     if (orders.length < orIndex || !(orders[orIndex]["ID"])) if (isFn(fn)) fn(orders);
//     const url =  store.getters.appBookUrlWuhu;    
//     const body = store.getters.appBodyWuhu({"act": 'get_unfinishorder_item',"OrderID":orders[orIndex]["ID"]});
    
//     const data = await fetchData({url, body});
    
//     var isErr = data.detail && data.detail.ErrorCode && data.detail.ErrorCode != "0"; 
//     //store.state.isLoading = false;
//     if (!isErr && !data.error){
//         //if (isFn(fn)) fn(data);
//         orders[orIndex]["items"] = [];
//         if (Array.isArray(data.OrderItem)) {
//             orders[orIndex]["items"] = data.OrderItem || [];
            
//         }
//     }else{
//         //msgShort(data.detail.ErrorMsg);
//     }
//     const next_index = orIndex+1;
//     if (orders.length > next_index){
//         getUnfinishorderItem(orders,next_index,fn);
//     }else{
//         store.state.isLoading = false;
//         if (isFn(fn)) fn(orders);
//     }
    
// }
async function cancelUnfinishorder(orderID,fn){
    //取消未付款的單,
    //因為全部order list,要記錄,之前撈過的items??
    store.state.isLoading = true;
    const url =  store.getters.appBookUrlWuhu;    
    const body = store.getters.appBodyWuhu({"act": 'cancel_unfinishorder',"OrderID":orderID});
    
    const data = await fetchData({url, body});
    
    var isErr = data.detail && data.detail.ErrorCode && data.detail.ErrorCode != "0"; 
    store.state.isLoading = false;
    if (!isErr && !data.error){
        if (data.ErrorMsg) showAlert(data.ErrorMsg);
        if (isFn(fn)) fn(data);
       
    }else{
        msgShort(data.detail.ErrorMsg);
    }
    
    
}
async function getVipAddr(fn) {        
    store.state.isLoading = true;
    const url =  store.getters.appBookUrlWuhu;    
    const body = store.getters.appBodyWuhu({"act": 'get_transport'});
    const data = await fetchData({url, body});
    var isErr = data.detail && data.detail.ErrorCode && data.detail.ErrorCode != "0"; 
    store.state.isLoading = false;
    if (!isErr && !data.error){
        if (data.ErrorMsg) showAlert(data.ErrorMsg);
        if (isFn(fn)) fn(data.transport); 
    }else{
        msgShort(data.detail.ErrorMsg);
    }
    
    
}
async function setVipAddr(p_vip,fn) {        
    store.state.isLoading = true;    
    const url =  store.getters.appBookUrlWuhu;    
    const body = store.getters.appBodyWuhu({"act": 'set_transport',"GID":p_vip.GID,"Nick_Name":p_vip.Nick_Name,"Addr":p_vip.Addr,"Phone":p_vip.Phone});
    const data = await fetchData({url, body});
    var isErr = data.detail && data.detail.ErrorCode && data.detail.ErrorCode != "0"; 
    store.state.isLoading = false;
    
    if (!isErr && !data.error){
        if (data.ErrorMsg) showAlert(data.ErrorMsg);
        if (isFn(fn)) fn(data.transport); 
    }else{
        msgShort(data.detail.ErrorMsg);
    }
    
    
}
async function delVipAddr(p_vip,fn) {        
    store.state.isLoading = true;
    const url =  store.getters.appBookUrlWuhu;    
    const body = store.getters.appBodyWuhu({"act": 'del_transport',"GID":p_vip.GID});
    const data = await fetchData({url, body});
    var isErr = data.detail && data.detail.ErrorCode && data.detail.ErrorCode != "0"; 
    store.state.isLoading = false;
    if (!isErr && !data.error){
        if (data.ErrorMsg) showAlert(data.ErrorMsg);
        if (isFn(fn)) fn(data.transport); 
    }else{
        msgShort(data.detail.ErrorMsg);
    }
    
    
}
// async function getVipAddr_old(fn) {        
//     // APPMemberOneEntry.ashx
//     const url = store.getters.appVipUrl;
//     //const url = "https://8012.jinher.com.tw/Public/APPMemberOneEntry.ashx";
//     const body = store.getters.appBodyWuhu({"act": 'GetExtAddr',"Nick_Name":"",});
//     const data = await fetchData({url, body});
//     var isErr = data.detail && data.detail.ErrorCode && data.detail.ErrorCode != "0"; 
//     if (!isErr && !data.error){
//         if (isFn(fn)) fn(); 
//     }else{
//         msgShort(data.detail.ErrorMsg);
//     }
    
//     //console.log("shipData.....>>",this.shipData);
// }
// async function setVipAddr_old(p_obj,fn){
//     // APPMemberOneEntry.ashx
//     const url = store.getters.appVipUrl;
//     //const url = "https://8012.jinher.com.tw/Public/APPMemberOneEntry.ashx";
//     //"GID":找不到GID時,則api會生成gid(新增一筆),反之update       
//     // const body = store.getters.appBodyWuhu({"act": 'SetExtAddr',"Nick_Name":"帥大哥","Addr":"屏東市潮州","GID":"",});
//     const body = store.getters.appBodyWuhu(Object.assign({}, {"act": 'SetExtAddr'}, p_obj));
//     const data = await fetchData({url, body});
//     var isErr = data.detail && data.detail.ErrorCode && data.detail.ErrorCode != "0"; 
//     //console.log("data.....>>",data);
//     if (!isErr && !data.error){  
//         if (isFn(fn)) fn();                     
        
//     }else{
//         msgShort(data.detail.ErrorMsg);
//     }
// }
function newCart(new_obj,userCount, goCartfn){
    new_obj.Total = new_obj.Price;  
    store.state.mallData.foodQty[new_obj.MainID] = store.state.mallData.foodQty[new_obj.MainID] || 0;
    store.state.mallData.kindQty[new_obj.KindID] = store.state.mallData.kindQty[new_obj.KindID] || 0;
    if (userCount != undefined){
        new_obj.Count = userCount;       
        new_obj.Total = toDecimal(new_obj.Price * new_obj.Count);
    }else{
        new_obj.Count = 1;            
    } 
    
    sendNewCart(new_obj,(new_cart)=>{
        if (userCount != undefined){
            store.state.mallData.foodQty[new_obj.MainID] += userCount;
            store.state.mallData.kindQty[new_obj.KindID] += userCount;            
        }else{                      
            store.state.mallData.foodQty[new_obj.MainID]++;
            store.state.mallData.kindQty[new_obj.KindID]++;            
        } 
        // 
        // if (isFn(goCartfn)) {goCartfn();}else{store.state.mallData.shopping_cart.push(new_cart);}
        if (isFn(goCartfn)) {goCartfn(new_cart);}else{store.commit('addShopCart', new_cart);}
        
        
    }); 
    
}
function newItem(food){
    //套餐 IsMain,Parent,ID(會先生成uuid),MainID
    return {              
        CK_AgioCost: chkTypeStrToInt(food.CK_AgioCost || "false"),
        ChangePrice: chkTypeInt(food.ChangePrice),
        Count: 1,
        Discount: chkTypeStrToInt(food.Discount || "false"),
        AddCost:food.AddCost || 0,
        FoodID: food.styleFoodID || food.ID,
        FoodName: food.Name || "",
        ID: food.uuid || getuuid(),
        IsMain: food.IsMain || "0",
        KindID: food.Kind || "",
        MainID: food.MainID || "",
        Parent: food.Parent || "",
        Price: toDecimal(food.EntGuid ? food.EntPrice: (food.Price1 || 0)),
        ServCost: chkTypeStrToInt(food.ServCost || "false"),
        Special: food.Special || "",
        Total: 0,
        TotalDiscount: chkTypeStrToInt(food.TotalDiscount || "false"),
        tasteID:food.tasteID || [],
        addID:food.addID || [],
        subType:food.subType || "noSub",
        Taste:(Array.isArray(food.style) && food.style.length === 1 && food.style[0]["isNoStyle"]?'':(food.Taste=="無"?"":food.Taste)) || "",
        Add:food.Add || "",
        kindindex:food.kindindex || -1,
        foodindex:food.foodindex || -1,
        Memo:food.Memo || "",
        GroupNo:food.GroupNo || "",
        styleFoodID:food.styleFoodID || food.ID,
        PIC1:food.PIC1 || "",
        PYCode:food.PYCode || "",
        
    };

}
function totalAmount(){
    var total = 0;
    store.state.mallData.shopping_cart = store.state.mallData.shopping_cart || [];
    store.state.mallData.shopping_cart.map(function (obj) {
        obj.Total = toDecimal(obj.Total) || 0;
        total = total + obj.Total;
    });
    
    return total;	    
}
function objByTotal(p_list,p_key){
    if (!Array.isArray(p_list)) return 0;
    var total = 0;
    p_list.map(function (obj) {
        obj[p_key] = toDecimal(obj[p_key]) || 0;
        total = total + obj[p_key];
    });
    
    return total;	    
}
function incNumber(p_item,userCount,fn){    
    changeCart(newItem(p_item),true,userCount,undefined,fn);	    
}
function decNumber(p_item){
    changeCart(newItem(p_item),false);
}
function delItem(p_item,isDel,delFn){ 
    // changeCart(p_item,false,undefined,isDel);
    changeCart(p_item,false,undefined,isDel,undefined,delFn);
   
}
function addItem(p_item){ 
    if (chkTypeInt(p_item.BatchID) != 0){
        changeCart(p_item,true);
    }else{
        showToast("特殊商品,無法變更數量");
    }
    
}

function checkDeedAct(e,p_index,deeds,selectList,p_total){
    //even,deeds索引,deeds array,已選id的List,比對金額
    // var input = e.target.control; // 目前選取欄位

    var lastDeed=0;
    var input = e.target;
    if (e.target.control){input = e.target.control;}
    
    if (input.checked != undefined){
      var isChecked = input.checked===true;
      var allTtotal=0;
      var checkedItem = deeds.filter((po)=>{  
        if (selectList.indexOf(po.deed_no)>-1){
          allTtotal += toDecimal(po.points);
          return true;
        } 
        
      });
      var isShowLastDeed=true;
      if (isChecked){
        var nowSelect= deeds[p_index]; //(最後一次)現在勾的ID
        
                    
        var overTotal = allTtotal - toDecimal(nowSelect.points);
        // if (toDecimal(this.tmpOrder.Total) < overTotal){  
        
        if (toDecimal(p_total) < overTotal){
          setTimeout(() => {
            var q_index = selectList.indexOf(nowSelect.deed_no)
            if (q_index>-1) selectList.splice(q_index,1);
            
            input.checked = !isChecked;
          }, 0)
          isShowLastDeed=false;                       
          showToast("點數已足夠,請勿再勾選");
        }
        
        
      }
      
      lastDeed = toDecimal(p_total)-objByTotal(checkedItem,'points')
      lastDeed = (lastDeed<0?0:lastDeed);
      var str_intent = 'primary';
      var str_text = "尚需 "+lastDeed+" 點抵扣";
      if (lastDeed > 0) str_intent = 'danger'
      if (lastDeed == 0) str_text = '點數已足夠扣抵';
      if (isShowLastDeed) showToast(str_text, {intent: str_intent, icon: 'issue' })
    }
    return lastDeed;
}
function getuuid() {
    var d = Date.now();
    if (typeof performance !== 'undefined' && typeof performance.now === 'function') {
        d += performance.now();
        //use high-precision timer if available
    }
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = (d + Math.random() * 16) % 16 | 0;
        d = Math.floor(d / 16);
        return (c === 'x' ? r : r & 0x3 | 0x8).toString(16);
    });
}
function toDecimal(x, int_length){
    var sysVal = 0; //計算小數位
    int_length = int_length || sysVal;
    if (int_length > 10)
    int_length = 10;
    var size = Math.pow(10, int_length);
    var f = parseFloat(x);
    if (isNaN(f)) {
    return 0;
    }
    f = Math.round(x * size) / size;
    return f;
}
function chkTypeStrToInt(p_va){
    if (typeof p_va === 'number'){
        return p_va;
    }
    return (p_va != undefined && p_va.toLowerCase() == 'true') ? 1 : 0 ;    
}
function chkTypeInt(p_va){
    return isNaN(parseInt(p_va)) ? 0 : parseInt(p_va);
}
function chkTypeFloat(p_va){
    return isNaN(parseFloat(p_va)) ? 0 : parseFloat(p_va);
}
function foodSrc(id){  
    return store.state.mallData.imgUrl + 'newpos001' + '/' + id + '.jpg';
}

function cartQty(p_obj){ 
    var keys = Object.keys(p_obj);
    var p_sum = 0;
    keys.map(p_key=>{p_sum += p_obj[p_key];});
    return p_sum;
}
function foodCheck(foods,isSetFood){     
    return foods.map(p_food=>{
        p_food.ID = p_food.ID || p_food.MainID;  //品項ID
        p_food.Price = p_food.CurPrice;    //原價
        // p_food.Unit='件';
        // p_food.MaxCount=5;
        if (isSetFood && !store.state.mallData.foodInfo[p_food.ID]) store.state.mallData.foodInfo[p_food.ID] = p_food; 
        
        // p_food.Price1 = (p_food.Price1 != undefined?p_food.Price1:9999);  //售價

        //塞demo 規格
        // p_food.style = malltmp.foodMarket.map(p_style =>{
        //     var style_arr = [];
        //     Object.keys(p_style).map( p_key =>{
        //         if (p_key.indexOf("SpecID") == 0){
        //         var specKey = p_style[p_key];
        //         if (specKey && specKey != "") style_arr.push(malltmp.specName[specKey]);
                
        //         }
        //     });
            
        //     // return {ID:p_style.ID,Name:style_arr.join()};
        //     return {ID:p_food.ID+"-"+p_style.ID,Name:style_arr.join()}; //暫時加foodid來區別訂單ID
        // });
        return p_food;
    });
}

async function getFoodmarket(kindID,kIndex,fn){ 
    //取商品
    // console.log(kindID,"getFoodmarket",fn);
    if (!kindID) {if (isFn(fn)) fn([]);return [];}

    if (store.state.mallData.iv_foods[kindID]){
        store.state.mallData.kind[kIndex]["foods"] = store.state.mallData.iv_foods[kindID];
        store.state.mallData.kind[kIndex]["isLoad"] = true;
           
    }else{
        store.state.isLoading = true;
        const url =  store.getters.appBookUrlWuhu;        
        const body = store.getters.appBodyWuhu({"act": 'get_foodmarket',"FoodKind":kindID});
        const data = await fetchData({url, body});
        var isErr = data.detail && data.detail.ErrorCode && data.detail.ErrorCode != "0"; 
        store.state.isLoading = false;
        if (!isErr && !data.error){ 
            if (data.ErrorMsg) showAlert(data.ErrorMsg);
            if (Array.isArray(data.FoodMarket)){
                var p_food_init = sortPrice(foodCheck(data.FoodMarket,true));
                // Name
                store.state.mallData.iv_foods[kindID] = p_food_init;                
                store.state.mallData.kind[kIndex]["foods"] = p_food_init;
                store.state.mallData.kind[kIndex]["isLoad"] = true;
                store.commit('turnShopCart');
                // setTimeout(() => store.commit('turnShopCart'), 100);
                // store.state.mallData.isCarUI = !store.state.mallData.isCarUI;
            }
            
        }else{
            msgShort(data.detail.ErrorMsg);
        }
    }

    if (isFn(fn)) fn();
}
function sortPrice(p_arr){
    const isAsc = store.state.mallData.sortPriceType == 'sp1';
    const isDesc = store.state.mallData.sortPriceType == 'sp2';
    if (isAsc){
        p_arr =  p_arr.sort(function (a, b) {
          return toDecimal(a.Price1) > toDecimal(b.Price1) ? 1 : -1;
        });
    }else if(isDesc){
        p_arr =  p_arr.sort(function (a, b) {
          return toDecimal(a.Price1) < toDecimal(b.Price1) ? 1 : -1;
        });
    }
    return p_arr;
}
async function getFoodkindSingle(kindID,fn){ 
    //取小類
    if (!kindID) {if (isFn(fn)) fn([]);return [];}

    store.state.isLoading = true;
    const url =  store.getters.appBookUrlWuhu;        
    const body = store.getters.appBodyWuhu({"act": 'get_foodkind',"FLevel":kindID});
    const data = await fetchData({url, body});
    var isErr = data.detail && data.detail.ErrorCode && data.detail.ErrorCode != "0"; 
    store.state.isLoading = false;
    var p_data=[];
    if (!isErr && !data.error){ 
        if (data.ErrorMsg) showAlert(data.ErrorMsg);
        if (Array.isArray(data.FoodKind)){
            p_data = data.FoodKind; //也會有FoodKind2大類,先不管
        }
        
    }else{
        msgShort(data.detail.ErrorMsg);
    }
    if (isFn(fn)) fn(p_data);
    
}
async function getGroupitems(p_gid,fn){ 
    //取分組
    if (!p_gid) {if (isFn(fn)) fn([]);return [];}

    store.state.isLoading = true;
    const url =  store.getters.appBookUrlWuhu;        
    const body = store.getters.appBodyWuhu({"act": 'get_groupitems',"GID":p_gid});
    const data = await fetchData({url, body});
    var isErr = data.detail && data.detail.ErrorCode && data.detail.ErrorCode != "0"; 
    store.state.isLoading = false;
    var p_data=[];
    if (!isErr && !data.error){ 
        if (data.ErrorMsg) showAlert(data.ErrorMsg);
        if (Array.isArray(data.GroupItems)){
            p_data = foodCheck(data.GroupItems,true); //也會有FoodKind2大類,先不管
        }
        
    }else{
        msgShort(data.detail.ErrorMsg);
    }
    if (isFn(fn)) fn(p_data);
    
}

async function getFoodmarket_only(kindID,fn){ 
    //取商品
    // console.log(kindID,"getFoodmarket",fn);
    if (!kindID) {if (isFn(fn)) fn([]);return [];}
    if (store.state.mallData.iv_foods[kindID]) {if (isFn(fn)) fn(store.state.mallData.iv_foods[kindID]);}
    store.state.isLoading = true;
    const url =  store.getters.appBookUrlWuhu;        
    const body = store.getters.appBodyWuhu({"act": 'get_foodmarket',"FoodKind":kindID});
    const data = await fetchData({url, body});
    var isErr = data.detail && data.detail.ErrorCode && data.detail.ErrorCode != "0"; 
    store.state.isLoading = false;
    if (!isErr && !data.error){ 
        if (data.ErrorMsg) showAlert(data.ErrorMsg);
        if (Array.isArray(data.FoodMarket)) store.state.mallData.iv_foods[kindID] = foodCheck(data.FoodMarket,true);
        
    }else{
        msgShort(data.detail.ErrorMsg);
    }

    if (isFn(fn)) fn();
}
async function getFoodmarketDetail(foodID,kIndex,fIndex,fn){ 
    //取商品規格
    if (!foodID) {if (isFn(fn)) fn([]);return [];}
    var isLoad=false;
    if (isLoad){ 
    // if (store.state.mallData.iv_styles[foodID]){
        store.state.mallData.kind[kIndex]["foods"][fIndex]["style"] = store.state.mallData.iv_styles[foodID];
        store.state.mallData.kind[kIndex]["foods"][fIndex]["isLoad"] = true;           
    }else{
        store.state.isLoading = true;
        const url =  store.getters.appBookUrlWuhu;        
        const body = store.getters.appBodyWuhu({"act": 'get_foodmarket_detial',"MainID":foodID});
        const data = await fetchData({url, body});
        var isErr = data.detail && data.detail.ErrorCode && data.detail.ErrorCode != "0"; 
        store.state.isLoading = false;
        if (!isErr && !data.error){   
            if (data.ErrorMsg) showAlert(data.ErrorMsg); 
            if (Array.isArray(data.FoodMarket_Detail))  {
                store.state.mallData.iv_styles[foodID] = chkStyle(data.FoodMarket_Detail);
                store.state.mallData.kind[kIndex]["foods"][fIndex]["style"] = chkStyle(data.FoodMarket_Detail);
                store.state.mallData.kind[kIndex]["foods"][fIndex]["isLoad"] = true;
            }   
            
        }else{
            msgShort(data.detail.ErrorMsg);
        }   
    }
    
    
    if (isFn(fn)) fn(store.state.mallData.kind[kIndex]["foods"][fIndex]);
}
async function getFoodmarketDetail_nowItem(foodID,fn){ 
    //取商品規格
    if (!foodID) {if (isFn(fn)) fn([]);return [];}
   
    store.state.isLoading = true;
    const url =  store.getters.appBookUrlWuhu;        
    const body = store.getters.appBodyWuhu({"act": 'get_foodmarket_detial',"MainID":foodID});
    const data = await fetchData({url, body});
    var isErr = data.detail && data.detail.ErrorCode && data.detail.ErrorCode != "0"; 
    store.state.isLoading = false;
    if (!isErr && !data.error){   
        if (data.ErrorMsg) showAlert(data.ErrorMsg); 
        if (Array.isArray(data.FoodMarket_Detail)){            
            var p_temp = Object.assign({}, store.state.mallData.nowItem);
            store.state.mallData.iv_styles[foodID] = Object.assign([], chkStyle(data.FoodMarket_Detail));            
            p_temp.style = Object.assign([], chkStyle(data.FoodMarket_Detail));            
            p_temp.isLoad = true;
            store.commit('setShopNowItem', Object.assign({}, p_temp));
            store.state.mallData.isCarUI = !store.state.mallData.isCarUI;
        }   
        
    }else{
        msgShort(data.detail.ErrorMsg);
    }  
    
    
    if (isFn(fn)) fn(store.state.mallData.nowItem);
}
async function getFoodmarketDetail_only(foodID,fn){ 
    //取商品規格
    if (!foodID) {if (isFn(fn)) fn([]);return [];}
   
    store.state.isLoading = true;
    const url =  store.getters.appBookUrlWuhu;        
    const body = store.getters.appBodyWuhu({"act": 'get_foodmarket_detial',"MainID":foodID});
    const data = await fetchData({url, body});
    var isErr = data.detail && data.detail.ErrorCode && data.detail.ErrorCode != "0"; 
    store.state.isLoading = false;
    var p_data = [];
    if (!isErr && !data.error){   
        if (data.ErrorMsg) showAlert(data.ErrorMsg); 
        if (Array.isArray(data.FoodMarket_Detail)){            
            p_data = chkStyle(data.FoodMarket_Detail);  
            
        }   
        
    }else{
        msgShort(data.detail.ErrorMsg);
    }  
    
    
    if (isFn(fn)) fn(p_data);
}
async function getFoodmarketDetail_kind(foodID,kIndex,fIndex,fn){ 
    if (!foodID) {if (isFn(fn)) fn([]);return [];}
    var isLoad=false;
    if (isLoad){ 
    // if (store.state.mallData.iv_styles[foodID]){
        store.state.mallData.foodkind[kIndex]["foods"][fIndex]["style"] = store.state.mallData.iv_styles[foodID];
        store.state.mallData.foodkind[kIndex]["foods"][fIndex]["isLoad"] = true;
           
    }else{
        store.state.isLoading = true;
        const url =  store.getters.appBookUrlWuhu;        
        const body = store.getters.appBodyWuhu({"act": 'get_foodmarket_detial',"MainID":foodID});
        const data = await fetchData({url, body});
        var isErr = data.detail && data.detail.ErrorCode && data.detail.ErrorCode != "0"; 
        store.state.isLoading = false;
        if (!isErr && !data.error){  
            if (data.ErrorMsg) showAlert(data.ErrorMsg);  
            if (Array.isArray(data.FoodMarket_Detail)){
                store.state.mallData.iv_styles[foodID] = chkStyle(data.FoodMarket_Detail);        
                store.state.mallData.foodkind[kIndex]["foods"][fIndex]["style"] = chkStyle(data.FoodMarket_Detail);
                store.state.mallData.foodkind[kIndex]["foods"][fIndex]["isLoad"] = true;
            }
            
            
        }else{
            msgShort(data.detail.ErrorMsg);
        }    
    }
    
    if (isFn(fn)) fn(store.state.mallData.foodkind[kIndex]["foods"][fIndex]);
}
async function getFoodmarketDetail_home(foodID,kIndex,fIndex,fn){ 
    if (!foodID) {if (isFn(fn)) fn([]);return [];}   
    var isLoad=false;
    if (isLoad){ 
    // if (store.state.mallData.iv_styles[foodID]){
        store.state.mallData.home[kIndex]["type"][fIndex]["style"] = store.state.mallData.iv_styles[foodID];
        store.state.mallData.home[kIndex]["type"][fIndex]["isLoad"] = true;
        
    }else{
        store.state.isLoading = true;
        const url =  store.getters.appBookUrlWuhu;        
        const body = store.getters.appBodyWuhu({"act": 'get_foodmarket_detial',"MainID":foodID});
        const data = await fetchData({url, body});
        var isErr = data.detail && data.detail.ErrorCode && data.detail.ErrorCode != "0";  
        store.state.isLoading = false;
        if (!isErr && !data.error){  
            if (data.ErrorMsg) showAlert(data.ErrorMsg);
            if (Array.isArray(data.FoodMarket_Detail)){
                store.state.mallData.iv_styles[foodID] = chkStyle(data.FoodMarket_Detail);
                store.state.mallData.home[kIndex]["type"][fIndex]["style"] = chkStyle(data.FoodMarket_Detail);
                store.state.mallData.home[kIndex]["type"][fIndex]["isLoad"] = true;
            }
            
            
        }else{
            msgShort(data.detail.ErrorMsg);
        }
    }
    
    if (isFn(fn)) fn(store.state.mallData.home[kIndex]["type"][fIndex]);
}
function chkStyle(p_detail){
    
    var ok_detail= p_detail.map(p_style=>{
        var style_arr = [];
        var p_ID = p_style.MainID;//不知為何 MainID vs FoodID 後台給的規則不一樣,
        if (p_style.FoodID.length > p_style.MainID.length){p_ID=p_style.FoodID}
        Object.keys(p_style).map(p_key =>{
            //MainID //規格的商品ID
            //FoodID //主商品ID
            var str_spec = "SpecID";
            if (p_key.indexOf(str_spec) == 0){
                var specKey = p_style[p_key];
                //
                var specNum = p_key.substring(str_spec.length, p_key.length);
                var kindKey= "SpecKindID"+specNum;
                if (specKey && specKey != "") style_arr.push(store.state.mallData.iv_spec[p_style[kindKey]+"-"+specKey] || "無");
            
            }
            
            
        });
        if(style_arr.length == 0) {
            style_arr.push("無");
            p_style.isNoStyle=true;
        }
        p_style.ID = p_ID;
        p_style.Name = style_arr.join("，");
        p_style.styleQty = style_arr.length;
        return p_style; //暫時加foodid來區別訂單ID
    });
    
    return ok_detail;
}
async function getFoodKind(fn){ 
    store.state.isLoading = true;
    const url =  store.getters.appBookUrlWuhu;        
    const body = store.getters.appBodyWuhu({"act": 'get_foodkind',});
    const data = await fetchData({url, body});
    var isErr = data.detail && data.detail.ErrorCode && data.detail.ErrorCode != "0";   
    store.state.isLoading = false;       
    if (!isErr && !data.error){   
        if (data.ErrorMsg) showAlert(data.ErrorMsg);       
        store.state.mallData.foodkind2 = (Array.isArray(data.FoodKind2)?data.FoodKind2:[]) || []; 
        store.state.mallData.foodkind = (Array.isArray(data.FoodKind)?data.FoodKind:[]) || [];   
        
    }else{
        msgShort(data.detail.ErrorMsg);
    }
    if (isFn(fn)) fn();
}
async function getFoodFreight(fn){ 
    store.state.isLoading = true;
    const url =  store.getters.appBookUrlWuhu;        
    const body = store.getters.appBodyWuhu({"act": 'get_foodfreight',});
    const data = await fetchData({url, body});
    var isErr = data.detail && data.detail.ErrorCode && data.detail.ErrorCode != "0";   
    store.state.isLoading = false;       
    if (!isErr && !data.error){ 
        if (data.ErrorMsg) showAlert(data.ErrorMsg);  
        if (Array.isArray(data.FoodFreightKind) && Array.isArray(data.FoodFreight)){
             
            data.FoodFreightKind.map((freig)=>{
                store.state.mallData.foodFreight[freig.ID]=freig;
                store.state.mallData.foodFreight[freig.ID]["items"] = data.FoodFreight.filter((p_sub)=>{
                    p_sub.text = (p_sub.Fee === 0 ?"滿NT$"+p_sub.StartPaytotal+"以上":(p_sub.StartPaytotal==0?"未滿NT$"+p_sub.EndPaytotal:p_sub.StartPaytotal+"~"+p_sub.EndPaytotal))+"，"+(p_sub.Fee === 0 ?"免運":"運費NT$"+p_sub.Fee);
                    return freig.GID === p_sub.MGID;    
                });
                return freig;
            });
        }      
        
    }else{
        msgShort(data.detail.ErrorMsg);
    }
    if (isFn(fn)) fn();
}

function getSearchFoods(){
    var foods=[];
    Object.keys(this.$store.state.mallData.iv_foods).map( p_key =>{
        foods = foods.concat(this.$store.state.mallData.iv_foods[p_key]);
        
    });
    
    // store.state.mallData.iv_currentItem = foods;
    
    if (foods.length == 0){
        this.$store.state.mallData.home.map( p_obj =>{
            foods = foods.concat(p_obj["type"]);            
        });
    }
    store.state.mallData.iv_currentItem = foods;
}
function msgShort(str_msg){
    str_msg = str_msg || '資料取得異常';
    showToast((str_msg.length > 50?'資料取得異常':str_msg));
}
async function getHeadergroup(fn){ 
    store.state.isLoading = true;
    const url =  store.getters.appBookUrlWuhu;        
    const body = store.getters.appBodyWuhu({"act": 'get_headergroup',});
    const data = await fetchData({url, body});
    var isErr = data.detail && data.detail.ErrorCode && data.detail.ErrorCode != "0";   
    store.state.isLoading = false;        
    if (!isErr && !data.error){   
        if (data.ErrorMsg) showAlert(data.ErrorMsg);            
        store.state.mallData.Banner = (Array.isArray(data.Banner)?data.Banner:[]) || [];
        store.state.mallData.Header = (Array.isArray(data.Header)?data.Header:[]) || [];
        store.state.mallData.home = data.Header.filter(p_head=>{
            
            if (data[p_head.GID]){
                p_head.type = foodCheck(data[p_head.GID],true);
                return p_head;
            }
            
        });
        store.state.mallData.isCarUI = !store.state.mallData.isCarUI;
        
    }else{
        msgShort(data.detail.ErrorMsg);
    }
    if (isFn(fn)) fn();
}
async function getFoodSpec(fn){ 
    store.state.isLoading = true;
    const url =  store.getters.appBookUrlWuhu;        
    const body = store.getters.appBodyWuhu({"act": 'get_foodspec',});
    const data = await fetchData({url, body});
    var isErr = data.detail && data.detail.ErrorCode && data.detail.ErrorCode != "0";  
    store.state.isLoading = false;       
    if (!isErr && !data.error){   
        if (data.ErrorMsg) showAlert(data.ErrorMsg); 
        store.state.mallData.foodSpec = (Array.isArray(data.FoodSpec)?data.FoodSpec:[])  || []; //規格
        store.state.mallData.foodSpecKind = (Array.isArray(data.FoodSpecKind)?data.FoodSpecKind:[])  || [];   //規格類別 
        data.FoodSpecKind.map(p_kind=>{
            var groupSpec = data.FoodSpec.filter((p_sub)=>{return p_sub.MGID == p_kind.GID});
            groupSpec.map(p_spec=>{ 
                store.state.mallData.iv_spec[p_kind.ID+"-"+p_spec.ID] = p_spec.Name;    
            });     
                
        }); 
        
        
        
    }else{
        msgShort(data.detail.ErrorMsg);
    }
    if (isFn(fn)) fn();
}
function reloadShopType(to){

    if(to.query.type && to.query.type === 'product'){
        store.state.mallData.reloadProductFn=()=>{
            if (store.state.mallData.nowItem && store.state.mallData.nowItem.ID){                
                singlefoodmarket(store.state.mallData.nowItem.ID,(food_arrs)=>{                    
                    if (Array.isArray(food_arrs) && food_arrs.length > 0){
                        
                        store.state.mallData.nowItem = Object.assign({}, food_arrs[0]);
                        store.state.mallData.nowItem.style = [];
                        store.state.mallData.nowItem.isLoad = false;
                        store.state.mallData.iv_styles[store.state.mallData.nowItem.ID] = null;

                        getFoodmarketDetail_nowItem(store.state.mallData.nowItem.ID,()=>{
                            
                            // if (isFn(fn)) fn(dataSty);
                                
                        });
                    }else{
                        var p_temp = Object.assign({}, store.state.mallData.nowItem);
                        p_temp.isLoad=false;
                        store.commit('setShopNowItem', Object.assign({}, p_temp));
                        showAlert("此商品已下架或不存在");
                    }
                });

                
            }
            
            
        }
    }else if (to.query.goto!=undefined && to.query.kind2Index!=undefined) {
        store.state.mallData.reloadGoKindFn=()=>{
          store.state.mallData.kind.map((p_kind,kIndex)=>{
            store.state.mallData.iv_foods[p_kind.ID] = null;
            store.state.mallData.kind[kIndex]["foods"] = null;
            store.state.mallData.kind[kIndex]["isLoad"] = false; 
            var tmpFoods=[];
            for (var i = 0; i < 17; i++) {
              tmpFoods.push({Name:"..."}); //為了讓點餐頁類別可以置頂
              
            }
            if (!p_kind.isLoad) p_kind.foods = tmpFoods;
            // console.log("ccct>>",to);
            if (kIndex == to.query.goto) getFoodmarket(p_kind.ID,kIndex,()=>{
                if (to.query.goto == 0){
                    if (store.state.mallData.listenEL) {store.state.mallData.listenEL.scrollTop = 1;}//Android會把scrollTop變成20,改回1
                }else{
                    if (store.state.mallData.listenEL) {store.state.mallData.listenEL.scrollTop+=1;store.state.mallData.listenEL.scrollTop-=1;}
                }
                
            });
            
            
          });
        }
    }
}
export { toDecimal, incNumber, decNumber, chkTypeFloat, chkTypeInt,foodSrc, cartQty, delItem, addItem, totalAmount, foodCheck, 
    getFoodmarket,getFoodmarketDetail,getFoodmarketDetail_home,getFoodmarketDetail_kind,getFoodKind,getHeadergroup,getFoodSpec,
    getMyShopCart,cleanCart,deleteCart,getVipAddr,setVipAddr,delVipAddr,getOrderStatus,itmeQtyReload,goCheckout,quickCheckout,getMyorders,
    getOrderitems,cancelMyorder,getSearchFoods,getFoodFreight,goCalcuCheckout,finishCheckout,getFoodmarket_only,getUnfinishorder,getUnfinishorderItem,
    cancelUnfinishorder,getDeedpoints,objByTotal,singlefoodmarket,searchFood,getExpired,checkDeedAct,reloadShopType,acceptCoupon,checkCoupon,getFoodkindSingle,getGroupitems,getFoodmarketDetail_only,sortPrice }
