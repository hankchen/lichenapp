// vue basic lib
import Vue from 'vue'
import Vuex from 'vuex'
import VueCookies from 'vue-cookies'
Vue.use(Vuex)
Vue.use(VueCookies)

// 客製/第三方 lib
import md5 from 'md5'
import axios from 'axios'
import fetchData from '../libs/fetchData.js'

// import { getDistance } from 'geolib'
import routerKakar from 'src/appLichen/router'
// import routerWuhulife from 'src/appWuhulife/router'
import {GetJsonTest,GetJsonData1} from 'src/json1.js'	// 取得json數據
import { deviseFunction, ensureDeviseSynced, deviseSetLoginInfo, setRedirectPage } from 'src/libs/deviseHelper.js'
import { showAlert, setLoading } from 'src/libs/appHelper.js'
import { formatTime } from 'src/libs/timeHelper.js'
import { S_Obj, L_Obj, IsFn, IsMobile, GUID } from 'src/fun1.js'	// 共用fun在此!

const IsDev = S_Obj.isDebug 	//開發debug

// const LS1 = L_Obj.read('loginForm') || {}
// const IsHank = IsDev || /270485|378395/.test(LS1.account);	// @@test


// 企業會員卡相關資訊(所屬門店、卡號、儲值金額、會員等級、優惠券 ... etc)
function rawRemberData() {
  return {
    isLoaded: false,
    // 會員卡
    card_info: {},
    // 會員資料
    vip_info: {},
    // (可使用的) 優惠券清單
    ticketList: [],
    // (尚未能使用的) 優惠券清單
    notAvailableTickets: [],
    // (已轉贈的) 優惠券清單
    gaveTicketList: [],
    // 會員購買交易資料(有購買品項與總金額)
    purchaseRecord: [],
    // 會員積點交易資料(使用紀錄)
    expiredPoints: [],
    // (單一)優惠券明細資訊 e.g. 使用規則、條款、期限、適用範圍 ... etc
    ticketInfo: {
      // 規則代號
      currentTypeCode: '',
      currentRuleCode: '',
      // 使用 QRcode
      currentTicketGID: '',
      currentTicketQRcode: '',
      // 規則條款
      ruleData: {},
      // 適用商品
      usageFoods: [],
      // 適用門店
      usageShops: [],
    },
    activityInfo:{
      list:[],
      finish:[],
      actiDetail:{},
      awards:[],
    },
  }
}

// 企業公開資料相關
function rawPublicData() {
  return {
    // DB connect 位置
    connect: '9001',
    // 首頁
    index: {
      // 主畫面可滑動的 Banners
      slideBanners: [],
      // 固定的副 Banner
      subBanner: [],
    },
    // 品牌資訊(關於我們, 使用者條款, 常見問題 ... etc)
    brandInfo: [],
    // 最新消息
    news: { data: {}, types: {}, banners: {}, },
    // 菜單頁
    menu: {
      // 大類 (代號, 名稱, 清單)
      mainType: [], // [{ code: '', name: '',  list: {} }, { code: '', name: '',  list: {} }, ...]
      // 小類 (代號, 名稱, 清單)
      subType: [],  // [{ code: '', name: '',  list: {} }, { code: '', name: '',  list: {} }, ...]
    },
    // 門店資訊頁
    storeList: { store: [] },
    // 主企業號 ID & connect (用於線上點餐)
    MainEnterPriseID: '',
    MainConnect: '',
    myBookList: null, // 我的預約
    myBookStatusList: [], // 我的預約狀態 - S:已送出, P:處理中, Y:己確認, N:己取消
    currentProduct: {} // 目前商品
  }
}
// 商城共用資料相關
function rawMallData() {
  return {
    foodkind2:[], //大類
    foodkind:[],//中類全部,未分
    kind:[],  //次類,內有foods(次類中的品項)
    foodTaste:{}, //口味加料
    iv_foods:{},//小類下的所有品項
    iv_styles:{},//品項下的所有規格
    foodSpec:[],//規格
    foodSpecKind:[],//規格類別
    shipData:[],//收件人清單
    iv_spec:{},//品項規格
    orderstatus:{},//訂單狀態
    shopOrderTmp:{Total:0},//訂單主表
    orders:[], //訂單清單
    imgUrl: 'https://9001.jinher.com.tw/WebPos/images/Food/',
    nowItem:{},//當下所選品項
    spyIndex: 0,//規格index
    shopping_cart:[],//購物車
    foodQty:{},//品項計數
    kindQty:{},//品項類別計數
    isCarUI:false,
    Banner:[], //商城首頁橫幅
    Header:[], //推薦商品群組
    iv_currentItem:[], //所有商品
    foodInfo:{},//商品資料object
    foodFreight:{},//運費相關
    reloadShopFn:null,
    kind2Index:0, //記下目前進入的大類
    mallScrollTop:0,
    sortPriceType:'',
    // barcodeItem:[{ID:"T001",Name:"消費體驗高雄館-餐點消費",Price1:''},{ID:"T002",Name:"消費體驗高雄館-商品消費",Price1:''}],//由掃瞄生成的品項
  }
}
// 依據環境參數，來決定 API baseUrl 的值
const baseUrl 	= 'wuhu.jh8.tw'
	,Test8012 	= '8012test.jh8.tw'

let IsRunAutoTicketed = false

const state = {
  appSite: 'wuhu', //'kakar', // 系統參數: 站台
  promise: null, // 保存promise
  // 是否在殼裡面(判斷能否使用殼相關接口的依據)
  isDeviseApp: false,
  // 啟動/關閉 『讀取中』的動畫效果
  isLoading: false,
  loadingMsg: '',
  // 顯示手動輸入 QRcode 表格
  showInputQRCode: false,
  // 客製的彈窗 (用於 alert / confirm)
  customModal: {
    type: '', text: '', confirmText: '', cancelText: '',
    // confirm note
    confirmNoteTitle: '', // 附註標題
    confirmNoteChoice: [], // 附註選項
    confirmNoteChoosed: '', // 附註選項已選
    confirmNoteRemark: '', // 附註選了其他要手key的說明
    confirmNoteRemarkPlaceHolder: '', // 其他原因的place holder
    confirmNoteWarnMsg: '' // 警告訊息
  },
  // CSS 全域參數(用於判斷 CSS 是否設定 -webkit-overflow-scrolling: touch)
  // true => touch,
  // false => auto (幾乎只用於首頁)
  cssTouch: true,
  isPageName: '', //內頁名稱
  // API url
  api: {
    Url8012: `https://${Test8012}/public/AppDataVIP.ashx`,
    figUrl: `https://${baseUrl}/public/AppDataVIPOneEntry.ashx`,
    // 卡+ 主服務
    // kakarUrl: `https://${baseUrl}/Public/AppNCWOneEntry.ashx`,
    // 企業的公開資料
    publicUrl: `https://${baseUrl}/public/newsocket.ashx`,
    // 企業的會員資料
    memberUrl: `https://${baseUrl}/public/AppDataVIP.ashx`,	// +token
    // 企業的會員消費點數資料
    // consumerUrl: `https://${baseUrl}/public/AppDataVIP.ashx`,
    // 預約資料
    bookUrl: `https://${baseUrl}/public/AppBooking.ashx`,
    // 預約的token資料
    tokenUrl: `https://${baseUrl}/public/AppFamilyGourmet.ashx`,
    // 圖片的路徑
    picUrl: `https://${baseUrl}`,
    // 取會員已登記的一代卡資料
    jgssgUrl: `https://${baseUrl}/public/jgssgvip.ashx`,
    // 會員資料存取,例:收件人存取
    // vipUrl: `https://${baseUrl}/public/APPMemberOneEntry.ashx`,
  },
  // 基本資訊
  baseInfo: {
    // 英文常稱
    isFrom: 'lichen2app',
    // 企業號
    EnterpriseID: '8493800757',
    // FunctionID
    FunctionID: '450701', // 捷利functionid=450701
    // 是否接收推播訊息 (跟殼要)
    pushNotify: '1', // 預設要開啟, 1:開啟推播, 0:關閉推播
    // GPS 資訊 (跟殼要)
    gps: {},
    // 螢幕亮度值 (跟殼要, range 0 ~ 255 預設為中間值)
    brightness: '120',
    // 前端打包檔版本號 (跟殼要)
    WebVer: '',
    // 殼的裝置 (iOS / Android)
    AppOS: '',
    // 殼是否有新版本
    DeviseVersionIsDiff: false,
    // (給線上點餐用) 回到 卡+ 時導向到指定企業 app
    currentEnterPriseID: (VueCookies.get('currentEnterPriseID') || ''),
    // 企業公開資料相關 (五互只需要rawPublicData()的這些欄位)
    qrcodeData:{},
    publicData: {
      // LOGO 圖檔
      logo: '',
      logoLoaded: false,
      advertise: {
        adData: {}
      },
      companyData: [], // 公司資料: 隱私權政策,關於我們...
      // 首頁
      index: {
        // 主畫面可滑動的 Banners
        slideBanners: [],
        // 固定的副 Banner
        subBanner: [],
        slideBannersUpdate: false, // 主橫幅的圖要立即更新
        subBannersUpdate: false,   // 副橫幅的圖要立即更新
      },
      AppName: '',
      // 最新消息
      news: { data: {}, types: {}, banners: {}, },
      // 門店資訊頁
      storeIsLoaded: false,
      storeList: { store: [] },
      // === 門店搜尋條件 === start
      storeSearch: {
        searchBrand: '全部品牌',
        searchZoneDisplay: '全部地區',
        searchZoneType: '',
        searchCity: '',
        searchArea: '',
        searchKeyWord: '',
        store2Store: false // 在store頁,透過footer再度到store頁
      },
      // === 門店搜尋條件 === end
      brandList:[],//當下所選品牌下的品項
      brandLink:[],//品牌下的社群
      brandNews:[],//品牌下的消息
      brandNewsOne: {},
      cloudsearch:[],//搜尋列
      brandListKind:[],//當下所選品牌下的品項,kind
      brandListDetail:[],//當下所選品牌下的品項,detail
    },
    // 企業會員卡相關資訊(所屬門店、卡號、儲值金額、會員等級、優惠券 ... etc)
    memberData: {
      vip_infoIsLoaded: false,
      // 會員資料
      vip_info: {},
      // 會員卡
      card_info: {},
      // 已登記的一代卡資料
      registeredCards: [],
      // (可領用的) 優惠券清單
      drawTicketList: [],
      // 會員儲值交易資料(使用紀錄)
      depositRecordIsLoaded: false,
      depositRecord: [],
      // 積分換卷的資料
      pointToTickets: [],
      // 年度總積點資料
      expiredPoints_section: [],
      // 會員積點交易資料(使用紀錄)
      expiredPointsIsLoaded: false,
      expiredPoints: [],
      // (可使用的) 優惠券清單
      ticketList: [],
      // (尚未能使用的) 優惠券清單
      notAvailableTickets: [],
      // (已轉贈的) 優惠券清單
      gaveTicketList: [],
      // (單一)優惠券明細資訊 e.g. 使用規則、條款、期限、適用範圍 ... etc
      ticketInfo: {
        // 規則代號
        currentTypeCode: '',
        currentRuleCode: '',
        // 使用 QRcode
        currentTicketGID: '',
        currentTicketQRcode: '',
        // 規則條款
        ruleData: {},
        // 適用商品
        usageFoods: [],
        // 適用門店
        usageShops: [],
      },
      // 會員購買交易資料(有購買品項與總金額)
      purchaseRecord: [],
      consumerPoints: [], 			// 全部消費點數契約清單
      consumerPointDetail: {}, 	// 單一筆契約消費點數紀錄
      oneOrderDetail: [], 			// 單一筆訂單購物清單
      rememberDates: [], 				// 重要的日子
      pushNotifys: [], 					// 推播記錄
      pushNotifyNoRead: 0, 			// 推播記錄-未讀
    }
  },
  // 簡訊認證設定 (需要從殼取)
  SMS_Config: {
    // 簡訊回傳的驗證碼
    reCode: "",
    // 每天最多簡訊次數 (預設為 5 次)
    APPSMSDayTimes: '5',
    // 每封簡訊發送間隔 (預設為 50 秒)
    AppSMSIntervalMin: '50',
    // 每日簡訊發送扣打 (用來紀錄已寄出幾封, 不得超過 APPSMSDayTimes 的值), 格式： yyyymmdd-times => e.g. 20190513-4 (05/13 已用四次)
    SMSSendQuota: '',
  },
  // 會員接口相關資訊
  member: {
    mac: IsMobile()?'':(VueCookies.get('kkMAC') || ''),
    Tokenkey: IsMobile()?'':(VueCookies.get('kkTokenkey') || ''),
    code: IsMobile()?'':(VueCookies.get('kkUserCode') || ''),
    pwd: IsMobile()?'':(VueCookies.get('kkUserPwd')||''),
  },
  // 首頁上面的卡面搜尋 bar
  appFilter: '',
  // 企業 app 的點擊次數統計
  appClicks: VueCookies.get('kkAppClicks') || {},
  vipAppsLoaded: false,
  // 已綁定的企業 app 列表
  vipApps: [],
  // 已綁定的企業 user 資料
  vipUserData: [],
  // 目前的進入/操作的 app
  currentAppUser: VueCookies.get('kkAppUser') || {},
  // (卡卡)會員帳號資訊
  userData: VueCookies.get('kkUserData') || {},
  // 企業會員卡相關資訊(所屬門店、卡號、儲值金額、會員等級、優惠券 ... etc)
  memberData: rawRemberData(),
  // 企業公開資料相關
  publicData: rawPublicData(),
   // 商城公開資料相關
  mallData: rawMallData(),

  // 卡片條碼 QRcode
  cardQRCode: { image: '', value: '', loaded: false },
  cardListScroll2: 0,
  fontSizeBase: 1,
  becomeApp:{isShow:false,isLoad:false},
}

const mutations = {
  /** 暫存字體大小 */
  setFontSizeTemp(state, fontSize) {
    if (!fontSize || isNaN(fontSize) || parseFloat(fontSize)<=0) {
      fontSize = 1 // 預設1
    }
    let floatFontSize = parseFloat(fontSize)
    state.fontSizeBase = floatFontSize
  },
  /** 永久保存字體大小 */
  setFontSizeForever(state, fontSize) {
    if (!fontSize || isNaN(fontSize) || parseFloat(fontSize)<=0) {
      fontSize = 1 // 預設1
    }
    let floatFontSize = parseFloat(fontSize)
    state.fontSizeBase = floatFontSize
    // 存入cookie
    VueCookies.set('fontSize', floatFontSize)
    // 若在殼中 => 存入殼裡
    if (IsMobile()) {
      let setThing = `{"spName":"fontSize", "spValue": ${ floatFontSize }}`
      deviseFunction('SetSP', setThing, '')
      // console.log('===> 已存入殼裡 =>' + setThing)
    }
  },
  // 存入系統參數：站台 & 基礎參數
  setAppSite(state, site) {
    state.appSite = site
    // 如果 site === 'wuhulife' , 要修改對應的 baseInfo.isFrom, baseInfo.EnterpriseID
    // if (site === 'wuhulife' ) state.baseInfo.isFrom = 'wuhulifeapp'
    // if (site === 'wuhulife' ) state.baseInfo.FunctionID = '450301'
    // if (site === 'wuhulife' ) state.baseInfo.EnterpriseID = '53239538'
    // if (site === 'wuhulife' ) state.baseInfo.AppName = '龍海就是消費'
  },
  //存入讀取的QRcode結果
  // setQrcodeData(state, data) { state.qrcodeData = data },
  // 存入當下進入的企業 app 資料
  setCurrentAppUser(state, app) { VueCookies.set('kkAppUser', app); state.currentAppUser = app },
  // 清空當下進入的企業 app 資料
  clearCurrentAppUser(state) { VueCookies.remove('kkAppUser'); state.currentAppUser = {} },
  // 存入企業 app 列表的點擊紀錄 (整個 object)
  saveAppClicks(state, clicks) { state.appClicks = clicks },
  // 存入企業 app 列表的點擊紀錄 ( EnterpriseID += 1 )
  setAppClicks(state, eID) {
    state.appClicks[eID] = state.appClicks[eID] || 0
    state.appClicks[eID] += 1
    // vue 的雷： object & array 的變化，需要用下面方式才會觸發 computed / getters
    // ref: https://github.com/vuejs/vuex/issues/1311
    Vue.set(state.appClicks, eID, state.appClicks[eID])
    // 存入 cookies
    VueCookies.set('kkAppClicks', state.appClicks)
    // 存入殼裡
    deviseFunction('SetSP', `{"spName":"appClicks", "spValue": ${ JSON.stringify(state.appClicks) }}`, '')
  },
  // 存入會員擁有的企業 app 使用者資料 (重複的就刷新點數資訊, 卡片資訊)
  setVipUserData(state, app) {
    const userData = state.vipUserData.find(item => item.EnterPriseID === app.EnterPriseID)
    const notFound = userData === undefined

    if (notFound) {
      state.vipUserData.push(app)
    }
    if (!notFound) {
      userData.cardPoint = app.cardPoint
      userData.CardTypeCode = app.CardTypeCode
      userData.CardTypeName = app.CardTypeName
      userData.CardFacePhoto = app.CardFacePhoto
    }
  },
  setVipUserLoaded() { state.vipAppsLoaded = true },
  // 設定 cssTouch
  setCssTouch(state, status){ state.cssTouch = status },
  // 設定 商城目前scrollTop
  setMallScrollTop(state, value){ state.mallData.mallScrollTop = value },
  // 設定是否在殼裡面
  setIsDeviseApp(state, status) { state.isDeviseApp = status },
  // 『讀取中...』 切換
  setLoading(state, data) {
    state.isLoading = data.status
		/* 追加顯示-載入中訊息 */
		var msg = ''
		switch (data.mType) {
		case 1:
			msg = '資料處理中，請稍候...'
			break;
		case 2:
			msg = '資料加載中，請稍候...'
			break;
		case 3:
			msg = '急速加載中...'
			break;
		case 4:
			msg = '努力搶券中，請稍候...'
			break;
		// default:
		}
		state.loadingMsg = msg
  },
  // 『顯示手動輸入 QRcode 表格』切換
  setInputQRCode(state, status) { state.showInputQRCode = status },
  // 設定彈跳視窗的資料 (顯示 / 隱藏)
  setCustomModal(state, { type, text, cancelText, confirmText,
    confirmNoteTitle, confirmNoteChoice, confirmNoteChoosed, confirmNoteRemark,
    confirmNoteRemarkPlaceHolder, confirmNoteWarnMsg,
    resolve, reject }) {
    cancelText = cancelText || '取消'
    confirmText = confirmText || '確認'

    state.customModal = { type, text, cancelText, confirmText,
      confirmNoteTitle, confirmNoteChoice, confirmNoteChoosed, confirmNoteRemark,
      confirmNoteRemarkPlaceHolder, confirmNoteWarnMsg}

    if (resolve && reject) state.promise = { resolve, reject }
  },
  // 存入已綁定的企業 app 列表
  setVipApps(state, payload) { state.vipApps = payload },
  // 存入手機簡訊認證的設定
  setSMS_Config(state, data) { state.SMS_Config = Object.assign({}, state.SMS_Config, data) },
  // 設定簡訊驗證碼
  setSMSreCode(state, v1) {
		// (S_Obj.isStaging && v1) && alert('簡訊驗證碼: \n'+JSON.stringify(v1));	// @@test
		state.SMS_Config.reCode = v1
  },
  // 存入基本資訊
  setBaseInfo(state, data) { state.baseInfo = Object.assign({}, state.baseInfo, data) },
  // 更新會員資訊
  setUserData(state, data) { state.userData = data },

  /** @NoUse: 不需要,因setLoginInfo己全蓋... */
  // setLoginInfoRtAPIUrl(state, data){
    // state.userData.RtAPIUrl = data.RtAPIUrl || "";
  // },

  // 存入Mac (在沒有登入的狀態)
  setMacNoLogin(state, mac) {
    // 存入 store
    state.member.mac = mac
  },
  // 存入會員登入資訊
  setLoginInfo(state, payload) {
    // payload sample: {data: data, rememberMe: true, pwd: 'xxx'}
    const data = payload.data
    console.log('存入會員登入資訊: ', data)
    console.log('rememberMe, pwd => '+payload.rememberMe, payload.pwd)

    // 存入 cookies
    if (payload.rememberMe) {
		console.log('===> is payload.rememberMe')
		console.log('setLoginInfo ===> save to cookies')
		VueCookies.set('kkMAC', data.mac)
		VueCookies.set('kkTokenkey', data.Tokenkey)
		VueCookies.set('kkUserData', data)
		VueCookies.set('kkUserCode', data.Account)
		VueCookies.set('kkUserPwd', payload.pwd)
    }
    // 存入 store
    state.userData = data
    state.member.mac = data.mac
    state.member.Tokenkey = data.Tokenkey
    state.member.code = data.Account
    state.member.pwd = payload.pwd

    // 依據登入參數導向指定頁面
    if (data.EnterPriseID === "8493800757") setRedirectPage()
  },
  // 清除企業 app 的會員資料
  clearAppMemberData(state) { state.memberData = rawRemberData() },
  // 清除企業 app 的公開資料
  clearAppPublicData(state) { state.publicData = rawPublicData() },
  // 會員登出
  setLogout(state) {
    VueCookies.remove('kkJWT')
    VueCookies.remove('kkMAC')
    VueCookies.remove('kkTokenkey')
    VueCookies.remove('kkUserData')
    VueCookies.remove('kkUserCode')
    VueCookies.remove('kkUserPwd')
    VueCookies.remove('kkUserJWT')

		L_Obj.del('userData');
	// L_Obj.clear();	// nono:有存loginForm

    state.vipApps = []
    state.userData = {}
    state.vipUserData = []
    // 清除會員基本資訊
    state.member.mac = ''
    state.member.Tokenkey = ''
    state.member.code = ''
    state.member.pwd = ''

    // === 此console不可以移除 === start
    let msg123 = '===> state.member =>' + JSON.stringify(state.member)
      + '<=== cookie => '
      + 'tokenkey=>' + VueCookies.get('kkTokenkey')
      + '<= kkJWT =>' + VueCookies.get('kkJWT')
      + '<= kkMAC =>' + VueCookies.get('kkMAC')
      + '<= kkUserData =>' + VueCookies.get('kkUserData')
      + '<= kkUserCode =>' + VueCookies.get('kkUserCode')
      + '<= kkUserPwd =>' + VueCookies.get('kkUserPwd')
      + '<= kkUserJWT =>' + VueCookies.get('kkUserJWT')

    console.log(msg123)
    // alert(msg123)
    // === 此console不可以移除 === end
  },
  // 存入企業 app 的 LOGO 資訊
  setAppLogo(state, data) { state.publicData.logoLoaded = true ;state.publicData.logo = data },
  // 存入企業 app 的 LOGO 資訊 - 五互
  setAppLogoWuhu(state, data) { state.baseInfo.publicData.logoLoaded = true ;state.baseInfo.publicData.logo = data },
  // 存入我的預約
  setMyBookList(state, data) {
    if (data) {
      // 手動加入readMore開闔欄位
      data.forEach((item)=>{
        item.readMore = false // 預設是闔
      })
    }
    state.baseInfo.publicData.myBookList = data
  },
  // 存入我的預約
  setMyBookStatusList(state, data) {
    state.baseInfo.publicData.myBookStatusList = data
  },
  // 存入企業 app 的 db connect 資訊
  setAppConnect(state, data) { const connect = data || '9001' ; state.publicData.connect = connect },
  // 存入首頁主畫面可滑動的 Banners - 全餐
  setAdvertise(state, data) {
    // console.log('setAdvertise ===> data =>', data);		// @@
    // === only for test === start
    // data = {"ADBanner":[],"SUBanner":[]}
    // === only for test === end
    state.baseInfo.publicData.advertise.adData = data
  },
  setCompanyData(state, data) { state.baseInfo.publicData.companyData = data },
  //存入品牌下的商品
  setBrandData(state, data) { state.baseInfo.publicData.brandList = data },
  //存入品牌下的商品,by kindid
  setBrandDataKind(state, data) { state.baseInfo.publicData.brandListKind = data },
  //存入品牌下的商品 by detailid
  setBrandDataDetail(state, data) { state.baseInfo.publicData.brandListDetail = data },

  //存入品牌下的社群
  setBrandLink(state, data) { state.baseInfo.publicData.brandLink = data},
  //存入品牌下的消息
  setBrandNews(state, data) { state.baseInfo.publicData.brandNews = data},
  //存入品牌下的某一筆消息
  setBrandNewsOne(state, data) {
    if (data && data.length>0) {
      state.baseInfo.publicData.brandNewsOne = data[0]
    }
  },
  //存入搜尋列設定
  setCloudsearch(state, data) { state.baseInfo.publicData.cloudsearch = data },
  // 存入首頁主畫面可滑動的 Banners - 五互
  setIndexSlideBannersWuhu(state, data) { state.baseInfo.publicData.index.slideBanners = data },
  // 存入首頁固定的副 Banner
  setIndexSubBanner(state, data) { state.publicData.index.subBanner = data },
  // 五互 - 存入首頁固定的副 Banner
  setIndexSubBannerWuhu(state, data) { state.baseInfo.publicData.index.subBanner = data },
  // 存入企業門店清單
  setStoreListData(state, payload) {
    const {stores, MainEnterPriseID, MainConnect} = payload
    state.publicData.MainEnterPriseID = MainEnterPriseID
    state.publicData.MainConnect = MainConnect
    state.publicData.storeList = stores
  },
  // 存入企業門店清單
  setStoreListDataWuhu(state, payload) {
    let {stores, storeMode, MainEnterPriseID, MainConnect} = payload
    state.publicData.MainEnterPriseID = MainEnterPriseID
    state.publicData.MainConnect = MainConnect
	// console.log('存入企業門店清單-storeMode: ', storeMode);	// @@

	if (storeMode.length && stores.store && stores.store.length) {
		stores.store = stores.store.map(one => {
			!one.Brand && console.error('Brand為空??: ', one);	// @@
			const mode = storeMode.find(m => m.ShopID === one.OrgCode)
			// console.log('setStoreListDataWuhu-mode: ', mode);	// @@
			one.Mode = mode ? JSON.parse(mode.Mode) : undefined;
			one.ShopID = mode ? mode.ShopID : undefined;
			return one;
		})
	} else {
		console.error('Error:微管雲未設門店清單', storeMode);
	}
	// console.log('存入企業門店清單-stores: ', stores);	// @@

    state.baseInfo.publicData.storeList = stores;
  },

  setStoreQueryBrand(state, payload) {
    state.baseInfo.publicData.storeSearch.searchBrand = payload
  },
  setStoreQueryZone(state, payload) {
    state.baseInfo.publicData.storeSearch.searchZoneDisplay = payload.searchZoneDisplay
    state.baseInfo.publicData.storeSearch.searchZoneType = payload.searchZoneType
    state.baseInfo.publicData.storeSearch.searchCity = payload.searchCity
    state.baseInfo.publicData.storeSearch.searchArea = payload.searchArea
  },
  setStoreQueryKeyWord(state, payload) {
    //console.log("payload>>>>",payload);
    state.baseInfo.publicData.storeSearch.searchKeyWord = payload
  },
  setStore2Store(state, payload) {
    state.baseInfo.publicData.storeSearch.store2Store = payload
  },
  // 主橫幅的圖片要即時更新
  setMainBannerUpdate(state, payload) {
    state.baseInfo.publicData.index.slideBannersUpdate = payload
  },
  // 副橫幅的圖片要即時更新
  setSubBannerUpdate(state, payload) {
    state.baseInfo.publicData.index.subBannersUpdate = payload
  },
  // 存入品牌資訊
  setBrandInfo(state, data) { if (data) state.publicData.brandInfo = data },
  // 存入最新消息的資料
  setNewsData(state, data) { if (!data.ErrorCode) state.publicData.news.data = data },
  // 存入最新消息的資料
  setNewsDataWuhu(state, data) { if (!data.ErrorCode) state.baseInfo.publicData.news.data = data },
  // 存入最新消息的分類
  setNewsTypes(state, data) { if (!data.ErrorCode) state.publicData.news.types = data },
  // 存入菜單的大類資料
  setMenuMainTypeListData(state, payload) { state.publicData.menu.mainType.find(item => {return item.code === payload.code}).list = payload.data},
  //存入全部小類下商品
  setFoodmarketData(state, data) {
    const foodCheck=(foods,isSetFood)=>{return foods.map(p_food=>{
      p_food.ID = p_food.ID || p_food.MainID;  //品項ID
      p_food.Price = p_food.CurPrice;    //原價
      if (isSetFood && !state.mallData.foodInfo[p_food.ID]) state.mallData.foodInfo[p_food.ID] = p_food;
      // p_food.Price1 = (p_food.Price1 != undefined?p_food.Price1:9999);  //售價

      return p_food;
    });}

    if (Array.isArray(data.foods)) {
      state.mallData.iv_foods[data.kindID] = foodCheck(data.foods,true);

      const findKind = (et) => et.ID == data.kindID;
      var kind_index = state.mallData.foodkind.findIndex(findKind);
      state.mallData.foodkind[kind_index]["foods"] = foodCheck(data.foods);
      // console.log("setFoodmarketData",data.kindID,state.mallData);
    }

  },
  setMenuMainTypeCodeName(state, data) {
    const noHaveData = (state.publicData.menu.mainType.find(item => {return item.code === data.code}) === undefined)
    if (noHaveData) state.publicData.menu.mainType = state.publicData.menu.mainType.concat( { code: data.code, name: data.name, list: [] } )
  },
  // 存入菜單的小類資料
  setMenuSubTypeListData(state, payload)  {state.publicData.menu.subType.find(item => {return item.code === payload.code}).list = payload.data},
  setMenuSubTypeCodeName(state, data) {
    const noHaveData = (state.publicData.menu.subType.find(item => {return item.code === data.code}) === undefined)
    if (noHaveData) state.publicData.menu.subType = state.publicData.menu.subType.concat( { code: data.code, name: data.name, list: [] } )
  },
  setCurrentProduct(state, data) {
    if (data && data.length>0) {
      state.publicData.currentProduct = data[0]
      state.isPageName = data[0].KindName // 分類中文名稱
    }
  },
  // 存入會員卡資訊
  setMemberVipData(state, data) {
		0 && console.log('不要秀bug: ', GetJsonTest);	// @@

    state.memberData.vip_info = data.querydata_return_info.vip_info
    state.memberData.card_info = data.querydata_return_info.card_info
    state.memberData.pointToTickets = data.PointToTickets // 積分換卷的資料
    state.memberData.expiredPoints_section = data.ExpiredPoints_section // 年度總積點資料
  },
  // 存入會員卡資訊 - 五互
  setMemberVipDataWuhu(state, data) {
    // === only for test 不同等級會員 === start
    // 再累積288元 即可升級 享金 會員
    // data.querydata_return_info.card_info.CardTypeCode = 'A'
    // data.querydata_return_info.card_info.DifferenceCumulativeSales = 288
    // data.querydata_return_info.card_info.CumulativeSales = 600
    // data.querydata_return_info.card_info.CardTypeCumulativeSales = 888

    // 再累積988元 即可升級 享鑽 會員
    // data.querydata_return_info.card_info.CardTypeCode = 'B'
    // data.querydata_return_info.card_info.DifferenceCumulativeSales = 988
    // data.querydata_return_info.card_info.CumulativeSales = 900
    // data.querydata_return_info.card_info.CardTypeCumulativeSales = 1888

    // 再累積588元 就能延續一年 享鑽 會員
    // data.querydata_return_info.card_info.CardTypeCode = 'C'
    // data.querydata_return_info.card_info.DifferenceCumulativeSales = 588
    // data.querydata_return_info.card_info.CumulativeSales = 1300
    // data.querydata_return_info.card_info.CardTypeCumulativeSales = 1888

    // 恭喜您成功延續一年 享鑽 會員
    // data.querydata_return_info.card_info.CardTypeCode = 'C'
    // data.querydata_return_info.card_info.DifferenceCumulativeSales = 0
    // data.querydata_return_info.card_info.CumulativeSales = 1900
    // data.querydata_return_info.card_info.CardTypeCumulativeSales = 1888
    // === only for test 不同等級會員 === end

		/** @@Test 積分換券 */
		// let json1 = await GetJsonTest('PointToTickets')	// @@
		// data.PointToTickets = json1

    state.baseInfo.memberData.vip_info = data.querydata_return_info.vip_info
    state.baseInfo.memberData.card_info = data.querydata_return_info.card_info
    state.baseInfo.memberData.pointToTickets = data.PointToTickets // 積分換卷的資料
    //console.log("優惠券總數data>>",state.baseInfo.memberData.vip_info.TicketCount);
    // === only for test start ===
    // data.ExpiredPoints_section = [{
				// ShopID: "TX07",
				// ShopName: "香繼光測試機B",
				// Points: "68.00",
				// ExpiredDate: "2020-12-31"
			// }, {
				// ShopID: "TX07",
				// ShopName: "香繼光測試機B",
				// Points: "6.00",
				// ExpiredDate: "2021-12-31"
			// }
    // ]
    // === only for test end ===

		let arr1 = data.ExpiredPoints_section

    // === 排序 start ===
    if (arr1.length) {
			// 排序: 由小到大
			arr1.sort(function (a, b) {
				if (a.ExpiredDate > b.ExpiredDate) {
					return 1
				} else {
					return -1
				}
			})
    }
    // === 排序 end ===
    // state.memberData.expiredPoints_section = arr1;	// 年度總積點資料
    state.baseInfo.memberData.expiredPoints_section = arr1 // 年度總積點資料

    // 2020-06-23 add: 重要的日子
    state.baseInfo.memberData.rememberDates = data.VIP_RemberDate
  },
  //簽到活動列表
  setVipActivityList(state, data) { state.memberData.activityInfo.list = data },

  setVipActivityListDetail(state, data) { state.memberData.activityInfo.actiDetail[data.ID] = data.list },
  //簽到已完成活動
  setVipActivityFinish(state, data) { state.memberData.activityInfo.finish = data },
  //得獎記錄
  setVipAwards(state, data) { state.memberData.activityInfo.awards = data },
  // 存入卡片條碼 QRcode
  setCardQRCodeData(state, data) { state.cardQRCode = data },
  // 存入會員的購買交易紀錄
  setMemberPurchaseRecordData(state, data) {
    state.baseInfo.memberData.purchaseRecord = data
  },
  // 存入可領用的優惠券列表資訊
  setMemberDrawTicketListData(state, data) {
    state.baseInfo.memberData.drawTicketList = data
  },
  // 存入優惠券列表資訊
  setMemberTicketListData(state, data) {state.memberData.ticketList = data},
  // 存入優惠券列表資訊 - 五互
  setMemberTicketListDataWuhu(state, data) {
    state.baseInfo.memberData.ticketList = data
  },
  // 存入已贈送的優惠券列表資訊
  setMemberGaveTicketListData(state, data) {state.memberData.gaveTicketList = data},
  // 存入已贈送的優惠券列表資訊 - 五互
  setMemberGaveTicketListDataWuhu(state, data) {
    state.baseInfo.memberData.gaveTicketList = data
  },
  // 存入尚未能使用的優惠券資料
  setMemberNotAvailableTickets(state, data) {state.memberData.notAvailableTickets = data},
  // 存入尚未能使用的優惠券資料 - 五互
  setMemberNotAvailableTicketsWuhu(state, data) {
    state.baseInfo.memberData.notAvailableTickets = data
  },
  // 存入會員的積點紀錄
  setMemberExpiredPointsData(state, data) { state.memberData.expiredPoints = data },
  // 存入會員的儲值交易紀錄
  setMemberDepositRecordData(state, data) {
    state.baseInfo.memberData.depositRecord = data
  },
  // 存入會員的積點紀錄
  setMemberExpiredPointsDataWuhu(state, data) { state.baseInfo.memberData.expiredPoints = data },
  // 存入會員的消費點數紀錄
  setMemberConsumerPointsDataWuhu(state, data) { state.baseInfo.memberData.consumerPoints = data },
  // 存入會員的消費點數紀錄
  setMemberConsumerPointDetailWuhu(state, data) {
    if (data) {
      // 手動加入readMore開闔欄位
      data.forEach((item)=>{
        item.readMore = false // 預設是闔
        item.orderDetail = null // 該筆訂單的購物清單
      })
    }

    state.baseInfo.memberData.consumerPointDetail = data
  },
  // 存入會員的單筆訂單紀錄
  setOneOrderDetail(state, data) {
    if (data) {
      let currentItem = state.baseInfo.memberData.consumerPointDetail[data.arrayIndex]
      if (currentItem) {
        currentItem.orderDetail = data.orderDetail
      }
    }
  },
  // 存入優惠券明細資訊 e.g. 規則條款、期限、適用範圍 ... etc
  setMemberTicketInfoData(state, data) {
    const ticket = state.baseInfo.memberData.ticketInfo
    // 規則代號
    if (data.currentTypeCode) ticket.currentTypeCode = data.currentTypeCode
    if (data.currentRuleCode) ticket.currentRuleCode = data.currentRuleCode
    // 使用規則、條款
    if (data.ruleData)   ticket.ruleData = data.ruleData
    // 適用商品
    if (data.usageFoods) ticket.usageFoods = data.usageFoods
    // 適用門店
    if (data.usageShops) ticket.usageShops = data.usageShops
  },
  // 存入-推播消息-全部內容
  setPushNotifyData(state, data) {
    state.baseInfo.memberData.pushNotifys = data
  },
  // 存入-推播消息-總數
  setPushNotifyNum(state, num) {
    // state.baseInfo.memberData.pushNotifys = data
			// let arr1 = data.filter((o1) => {return !o1.isRead})
// console.log('存入會員的推播記錄: ', arr1);	// @@
    state.baseInfo.memberData.pushNotifyNoRead = num
  },
  // 存入優惠券條碼 QRcode
  setTicketQRCodeData(state, data) {
    state.baseInfo.memberData.ticketInfo.currentTicketGID    = data.gid
    state.baseInfo.memberData.ticketInfo.currentTicketQRcode = data.image
  },
  // 存入殼的版本是否需要更新
  SetDeviseVersionIsDiff(state, status) {
    const value = (status === '1') ? true : false
    state.baseInfo.DeviseVersionIsDiff = value
  },
   // 存入購物車
  setShopCart(state, data) { state.mallData.shopping_cart = data },
   // 新增購物車
  addShopCart(state, data) { state.mallData.shopping_cart.push(data) },
  // 商城觸發UI更新用
  turnShopCart(state) {state.mallData.isCarUI = !state.mallData.isCarUI},
  // 商城分組商品更新
  groupFood(state, data) {state.mallData.home[data.fIndex]["type"] = data.GroupItems;},
   // 存入商品詳情頁
  setShopNowItem(state, data) {state.mallData.nowItem = data},
  // 商城存入小類商品
  setShopKindFoods(state, data) {state.mallData.kind[data.index]["foods"] = data.foods},
  // 商城存入訂單記錄
  setShopRecord(state, data) { state.mallData.orders = data },
  // 商城小類頁品項排序
  setSortPriceType(state, data) {state.mallData.sortPriceType = data},
  // 商城小類頁scrollListener
  setShoplistenELe(state, data) {state.mallData.listenEL = data},
  setBecomeApp(state) {
    state.becomeApp.isShow = !state.becomeApp.isShow
  },
  setBecomeApp_load(state) {
    state.becomeApp.isLoad = !state.becomeApp.isLoad
  },
  // 商城清除暫存
  clsShopMall(state) {
    state.mallData.foodkind2 = [];  //大類
    state.mallData.foodkind = [];   //小類
    state.mallData.iv_foods = {}; //小類下的所有品項
    state.mallData.iv_styles = {}; //品項下的所有規格
    state.mallData.foodInfo = {}; //by foodID 品項基本資料
  },
  // initQrCodeItem(state){
    // //由掃瞄生成的品項
    // state.mallData.barcodeItem = [{ID:"T001",Name:"消費體驗高雄館-餐點消費",Price1:''},{ID:"T002",Name:"消費體驗高雄館-商品消費",Price1:''}];
  // },

}

const getters = {
  getBaseUrl : () => {
    return baseUrl
  },
  fontSizeBase: state => {
    let fontSize = state.fontSizeBase
    if (!fontSize || isNaN(fontSize) || parseFloat(fontSize)<=0) {
      fontSize = 1 // 預設1
    }
    return parseFloat(fontSize)
  },
  // 站台 router
  router: () => {//state => {
    // if (state.appSite === 'wuhulife') return routerWuhulife
    return routerKakar
  },
  // 是否登入
  isLogin: state => {
    return !!(state.member.code && state.member.mac && state.member.Tokenkey);
  },
  // 卡卡的 requestBody
  kakarBody: state => {
    const {mac, code} = state.member
    const {isFrom, EnterpriseID, FunctionID} = state.baseInfo
    return { FunctionID, EnterpriseID, isFrom, mac, Account: code, }
  },
  // 企業 app 的 requestBody
  appBody: state => (body) => {
    const inputBody = ( body || {} )
    const {mac, isFrom, EnterPriseID} = state.currentAppUser
    const basicBody = {
      mac, isFrom,
      EnterpriseID: EnterPriseID,
      Account: state.member.code,
    }

    return Object.assign({}, basicBody, inputBody)
  },
  // 企業 app 的 requestBody
  appBodyWuhu: state => (body) => {
    const inputBody = ( body || {} )
    const {mac, Tokenkey, isFrom} = {
      mac: state.member.mac,
      Tokenkey: state.member.Tokenkey,
      isFrom: state.baseInfo.isFrom
    }
    const basicBody = {
      mac, isFrom, Tokenkey,
      EnterPriseID: state.baseInfo.EnterpriseID, // 注意2個P大小寫不同
      Account: state.member.code,
    }
    return Object.assign({}, basicBody, inputBody)
  },
  // 企業 app 的 requestBody
  appBodyWuhuEncode: state => (body, paramBody) => {
    const inputBody = ( body || {} )
    const {mac, Tokenkey, isFrom} = {
      mac: state.member.mac,
      Tokenkey: state.member.Tokenkey,
      isFrom: state.baseInfo.isFrom
    }

    // === param start ===
    const paramInputBody = (paramBody || {})
    let paramData = {
      Account: btoa(state.member.code)
    }
    let paramEndData = Object.assign({}, paramData, paramInputBody)
    // === param end ===

    const basicBody = {
      mac, isFrom, Tokenkey,
      EnterPriseID: state.baseInfo.EnterpriseID, // 注意2個P大小寫不同
      param: paramEndData,
    }

    return Object.assign({}, basicBody, inputBody)
  },
  // 目前的企業 App
  currentApp: state => {
    return state.vipApps.find(item => item.EnterpriseID === state.currentAppUser.EnterPriseID)
  },
  connect: (state) => {
    return state.publicData.connect
  },
  // 企業 app 的 預約資料 - 五互
  appBookUrlWuhu: (state) => {
    return state.api.bookUrl
  },
  // 企業 app 的 預約token資料 - 五互
  appTokenUrlWuhu: (state) => {
    return state.api.tokenUrl
  },
  // 企業 app 的 publicApiUrl
  appPublicApiUrl: (state, getters) => {
    return getters.currentApp.apiip.replace('/public/AppVIP.ashx', '/public/AppNCW.ashx')
  },
  // 靜態檔的 url
  srcUrl: (state, getters) => {
    return getters.currentApp.apiip.replace('/public/AppVIP.ashx', '')
  },

  // appVipUrl: (state) => {
    // //會員資料存取,例:收件人存取 by 20200225
    // return state.api.vipUrl
  // },
  // 取手機的螢幕亮度值
  deviseBrightness: state => {
    if (state.baseInfo.brightness <= 70) return 120
    return state.baseInfo.brightness
  },
  // 全餐廣告
  getAdvertise: (state) => {
    return state.baseInfo.publicData.advertise
  },
  //  五互[我的預約]歷史紀錄
  getMyBookList: (state) => {
    return state.baseInfo.publicData.myBookList
  },
  //  五互[我的預約]狀態清單
  getMyBookStatusList: (state) => {
    return state.baseInfo.publicData.myBookStatusList
  } //,
  // getCustomModal: (state) => {
  //   return state.customModal
  // }
}

const actions = {
  /* 卡卡資料相關 */
  // init 時執行所需的 action
  async fetchInitData() {
    // 1. 跟殼取資料
    await ensureDeviseSynced()
  },
  // (卡卡) 確認會員帳號是否未註冊
  async ensureMemberCodeNotRegistered({getters}, account) {
    const url = state.api.figUrl
    const body = getters.appBodyWuhu({
      act: 'memberchk',
      Account: account
    })
    const head = getters.requestHead
    const data = await fetchData({url, body, head})

    return new Promise(resolve => resolve(data))
  },
  // (卡卡) 會員註冊
  async ensureMemberRegister({state}, payload){
    const url = state.api.figUrl
    const body = Object.assign({}, {
      act: "memberreg",
      "isFrom": state.baseInfo.isFrom,
      "EnterpriseID": state.baseInfo.EnterpriseID
    }, payload)
    const head = { "headers": { "Content-Type": "application/x-www-form-urlencoded" } }
    const {data} = await axios.post( url, body, head )
    return new Promise(resolve => resolve(data))
  },
  // 撈沒有登入狀態的mac-無打api
  async getMacNoLogin({commit}) {
		commit('setMacNoLogin', S_Obj.mac)
  },
  /* 會員登入 */
  async memberLogin({state, commit}, formData) {
    setLoading(true);
    const {isFrom, EnterpriseID} = state.baseInfo
		const url = state.api.figUrl
    const pwd = md5(formData.password)
    const body = {
      isFrom, EnterpriseID,
      "act": "login",
      "memberphone": formData.account,
      "memberPwd": pwd,
      "FunctionID":"450702",
    }
    let data 	= await fetchData({url, body})
			,one		= data[0]
		// console.log('memberLogin-data: ', one);	// @@

		if (data.error) {
			setLoading(false);
			location.href = '#/login';

			showAlert(data.msg+'<div class="mt-2 is-size-6">* 若您是饗樂捷利的舊會員，請使用同一支手機號碼，重新註冊並登入。</div>')
			return
		}

		IsMobile() && L_Obj.save('loginForm', formData);

		/** @Remember: 針對測試人員,登入資料需RtAPIUrl(8012test)重新登入,才能拿到測試站資料 */
		const RtAPIUrl = one.RtAPIUrl
		if (RtAPIUrl) {
			const isFromTest = /8012test/.test(RtAPIUrl)
			if (isFromTest) {
				const reqUrl = state.api.Url8012
				const head = { "headers": { "Content-Type": "application/x-www-form-urlencoded" } }
				data = await axios.post(reqUrl, body, head)
				// data = await fetchData({reqUrl, body})	// nono:必須一定要改由8012test

				const data1 = data.data
					,hasOK 		= data1.ErrorCode == 0
				if (hasOK) {
					const req1 = JSON.parse(data1.Remark)[0]
					one = Object.assign({}, req1, {RtUrl: one.RtUrl, RtAPIUrl:one.RtAPIUrl})
					console.log('member2Login-data: ', one);	// @@
				} else {
					showAlert(data1.ErrorMsg);
				}
			}
		}
		L_Obj.save('userData', one);

		setLoading(false);

		/** @NoUse: 不需要,因setLoginInfo己全蓋... */
    // commit('setLoginInfoRtAPIUrl', one)//為了區別是在正式 or 測試登入,取token (API service不同)

    // => 存入會員資訊 (cookie, store)
    commit('setLoginInfo', {data: one, rememberMe: formData.rememberMe, pwd})
    // => 存入會員資訊 (殼)
    deviseSetLoginInfo({type: 'set', data: one, rememberMe: true, pwd})
		/** @NoNo: 勿在store內做導向,會有很多延伸問題...※有陷阱 */
    // 取完後再導向首頁
    // getters.router.push('/card')
  },
  // (卡卡) 會員登出
  memberLogout({commit, getters}) {
    // 清除 殼 裡的會員資料
    deviseSetLoginInfo({type: 'reset'})
    // 清除 state 裡的會員資料
    commit('setVipActivityFinish', [])
    commit('setLogout')
    // 導向登入頁
    getters.router.push('/login')
  },
  // 會員修改個人資訊
  async ensureMemberUpdateProfile({state, getters}, data){
    let url = 'https://' + baseUrl + '/Public/AppDataVIPOneEntry.ashx'
    let apiBody = Object.assign({}, data, {
      act: "memberupdate_wuhu",
      memberphone: state.member.code,
      FunctionID: "450501",
    })
    const body = getters.appBodyWuhu(apiBody)
    const update = await fetchData({url, body})

    return new Promise(resolve => resolve(update) )
  },
  // 使用積分換卷(後續邏輯由 call 此 action 的人處理)
  async ensurePointExchangeTicket({state, getters}, ticket) {
    setLoading(true)
    const url = state.api.memberUrl
    const body = getters.appBodyWuhu({
      act: "PointExchangeTicket",
      OpenID: state.member.code,
      TicketCount: "1",
      TicketTypeCode: ticket.TicketTypeCode,
      Points: ticket.Points
    })
    const data = await fetchData({url, body})
    setLoading(false)
	return !data.error ? data : data.detail;
  },
  // 優惠券領用
  async ensureFetchDrawTicket({state, getters}, DrawGID) {
    setLoading(true, 1);

    const url = state.api.memberUrl
			,now1 	= formatTime(new Date(), 'YYYYMMDDHHmm')
		// console.log('優惠券領用-now1: ', now1);	// @@
    const body = getters.appBodyWuhu({
      act: "GetDrawTicket",
      DrawGID: DrawGID,
      actTime: now1
    })
    const data = await fetchData({url, body})
    setLoading(false);
		return !data.error ? data : data.detail;
  },
  // 領取回家禮(享聚卡回娘家-綁定
  async ensureJgssgMemberCardBind({state, getters}, data){
    if (!getters.isLogin)		return

    const body = getters.appBodyWuhu({
      act: "viplichen",
      Account: data.Account,
      cardNo: data.cardNO,
      // validateCode: data.verifyNO
    })
		const isTestAPI	= S_Obj.isTestAPI()
			,url 					= isTestAPI ? state.api.Url8012 : state.api.figUrl
			,req 					= await fetchData({url, body})
    return new Promise(resolve => resolve(req) )
  },
  // 領取回家禮(享聚卡回娘家-檢查
  async ensureJgssgMemberCardCheck({state, getters}, data){
    if (!getters.isLogin)		return

    const body = getters.appBodyWuhu({
      act: "getviplichen",
      Account: data.Account,
      cardNo: data.cardNO,
    })

		const isTestAPI	= S_Obj.isTestAPI()
			,url 					= isTestAPI ? state.api.Url8012 : state.api.figUrl
			,req 					= await fetchData({url, body})

    return new Promise(resolve => resolve(req) )
  },
  async fetchQrcodUrl({dispatch, getters}, qrCode) {
    qrCode = qrCode.replace('＆','&');
    // console.log('fetchQrcodUrl-qrCode: ', qrCode);	// @@

		let _find		= 'shop?type=specShop&list='
			,arr1 		= qrCode.replace(_find, '').split(',')
			,codeID		= arr1[0]

		codeID && dispatch('GetCustomerCode', codeID).then(res2 => {
			// console.log('GetCustomerCode-res2: ', res2);	// @@
			if (res2) {
				getters.router.push({
					query: {
						type: 'specShop',
						ShopID: res2.ShopID,
						ShopName: res2.ShopName
					}
				})
			} else {
				showAlert('找不到此門店!');
			}
		})


  },
  /**
    統一用這個先將品牌資料撈好
  */
  async fetchVipAppsByCheck({dispatch, commit}, enterpriseID) {
    await dispatch('fetchVipAppsIfNoData', enterpriseID)
    const findApp = state.vipUserData.find(app => app.EnterPriseID === enterpriseID)
    commit('setCurrentAppUser', findApp)
  },
  /**
    refresh時只呼叫卡包清單和單一品牌
  */
  async fetchVipAppsIfNoData({dispatch}, enterpriseID) {
    let brandList = state.vipUserData
    // 有資料表示為正常點按,不用重撈
    if (brandList && brandList.length>0) {
      // console.log('===> brandList > 0')
      new Promise(resolve => {resolve(true)})
    // refresh
    } else {
      // console.log('===> brandList < 0')
      await dispatch('fetchVipAppsOnlyList', enterpriseID)
    }
  },
  /**
    只呼叫卡包清單和單一品牌
  */
  async fetchVipAppsOnlyList({dispatch, getters, commit}, enterpriseID) {
    if (!getters.isLogin)		return

    const url = `https://${baseUrl}/Public/AppVipOneEntry.ashx`
    const body = Object.assign({}, getters.kakarBody, { act: 'GetKKVipAPP' } )
    const apps = await fetchData({url, body})
    if (apps.error) return

    await commit('setVipApps', apps)
    // 完成使用者的基本資料載入
    await commit('setVipUserLoaded')
    // 將"單一" app 的會員資料存入
    let app = await apps.find(curApp => {
      return curApp.EnterpriseID === enterpriseID
    })
    await dispatch('fetchVipAppData', {app: app})
  },
  // (卡卡) 取已綁定的企業 app 列表
  async fetchVipApps({dispatch, getters, commit}) {
    if (!getters.isLogin)		return
    const url = `https://${baseUrl}/Public/AppVipOneEntry.ashx`
    const body = Object.assign({}, getters.kakarBody, { act: 'GetKKVipAPP' } )
    const apps = await fetchData({url, body})

    if (apps.error) return

    // 存入已綁定的企業 app
    commit('setVipApps', apps)
    // 完成使用者的基本資料載入
    commit('setVipUserLoaded')
    // 將各 app 的會員資料存入
    apps.forEach(app => dispatch('fetchVipAppData', {app: app}) )
  },
  // (卡卡) 取綁定企業的基本資料
  async fetchVipAppData({state, getters, commit}, payload) {
    if (!getters.isLogin)		return

    const { app } = payload
    const { EnterpriseID, isFrom, mac } = app

    // 取會員的點數與卡資料
    const url = state.api.memberUrl
    const body = {
      act: "GetVipAndCardInfo",
      Account: state.member.code,
      mac, isFrom, EnterpriseID,
    }
    const vipCardInfo = await fetchData({url, body})
    if (vipCardInfo.error) return

    // 將取得的資料存入入 userData 裡面
    const ticketData = vipCardInfo.querydata_return_info.ticket_info
    const cardData   = vipCardInfo.querydata_return_info.card_info
    const vipData    = vipCardInfo.querydata_return_info.vip_info

    // 取企業會員的卡片圖檔
    let cardImg
    if (app.Dir && app.FileName) cardImg = 'https://' + baseUrl + app.Dir + app.FileName
    if (cardData.CardFacePhoto !== '') cardImg = app.apiip.replace('/public/AppVIP.ashx', '') + cardData.CardFacePhoto

    // 設定要存入的會員資料
    const userData = {
      Sex:           vipData.sex,
      mac:           app.mac,
      Name:          vipData.CnName,
      Email:         vipData.Email,
      isFrom:        app.isFrom,
      Address:       vipData.Addr,
      Mobile:        vipData.mobile,
      Account:       vipData.CardNO,
      Birthday:      vipData.BirthDay,
      ExpiredDate:   cardData.ExpiredDate,
      EnterPriseID:  app.EnterpriseID,
      CardID:        vipData.CardId,
      CardNO:        vipData.CardNO,
      cardGID:       cardData.GID,
      cardPoint:     parseInt(cardData.Points),
      cardTicket:    ticketData,
      CardTypeCode:  cardData.CardTypeCode,
      CardTypeName:  cardData.CardTypeName,
      CardFacePhoto: cardImg,
    }

    commit('setVipUserData', userData)
  },
	/** @NoUse: 捷利 = foWuhu */
  /* 取會員資料 */
  // async fetchVipAndCardInfo({getters, commit}) {
    // // 必須為登入狀態
    // if (!getters.isLogin) return

		// const url = state.api.memberUrl
    // const body = getters.appBody({act: 'GetVipAndCardInfo'})
    // const data = await fetchData({url, body})
    // if (data.error) return

    // commit('setMemberVipData', data)
  // },
  /* 取會員資料 */
  // async fetchVipAndCardInfoWuhu({getters, commit, state},p_data) {
  async fetchVipAndCardInfoWuhu({getters, commit}) {

    // console.log('fetchVipAndCardInfoWuhu ===> start')
    // 必須為登入狀態
    if (!getters.isLogin) return

    const url = state.api.memberUrl
			// ,_act 	= 'GetVipAndCardInfo'
    // const body = (p_data && p_data.type?getters.appBodyWuhu({act: _act,type:p_data.type}):getters.appBodyWuhu({act: _act}))
    const body = getters.appBodyWuhu({act: 'GetVipAndCardInfo'})
    const data = await fetchData({url, body})
    if (data.error) return
    state.baseInfo.memberData.vip_infoIsLoaded = true

    commit('setMemberVipDataWuhu', data)
  },
  //活動列表
  async fetchVipGetActivity({getters, commit}) {
    // 必須為登入狀態
    if (!getters.isLogin) return
    const url = state.api.memberUrl
    const body = getters.appBodyWuhu({act: 'getactivity'})
    const data = await fetchData({url, body})
    if (data.error) return
    commit('setVipActivityList', data)
  },
  //活動列表
  async fetchVipGetActivity_smp({getters, commit}) {

    if (getters.isLogin) return
    const url = `https://${baseUrl}/public/AppVIP.ashx`
    const body ={
      "mac":S_Obj.mac,
      "isFrom":"lichen2app","EnterPriseID":state.baseInfo.EnterpriseID,
      "Account":"","act":"getactivity"}
    const data = await fetchData({url, body})
    if (data.error) return
    commit('setVipActivityList', data)
  },
  //活動列表詳細
  async fetchVipGetActivityDetail({getters, commit},Info) {
    // 必須為登入狀態
    if (!getters.isLogin || !Info.GID) return
    const url = state.api.memberUrl
    const body = getters.appBodyWuhu({act: 'getactivitylist',ActivityID:Info.GID})
    const data = await fetchData({url, body})
    if (data.error) return
    commit('setVipActivityListDetail', {ID:Info.GID,list:data})
  },
  //活動簽到
  async fetchVipActivity({getters, commit},Info) {
    // 必須為登入狀態
    if (!getters.isLogin || !Info.GID) return
    const url = state.api.memberUrl
    const body = getters.appBodyWuhu({act: 'vipactivity',ActivityID:Info.GID})
    setLoading(true);
    const data = await fetchData({url, body})
    setLoading(false);
    let p_data = data;
    const isDetail = p_data.detail && p_data.detail.Remark && Array.isArray(JSON.parse(p_data.detail.Remark))

    if (p_data.error && !isDetail && p_data.msg) return showAlert(p_data.msg);
    if (isDetail){
      p_data = JSON.parse(p_data.detail.Remark);
      if (data.msg) p_data.ErrorMsg = data.msg;
    }
    if (!Array.isArray(p_data)) return new Promise(resolve => resolve(p_data))
    const p_fin = state.memberData.activityInfo.finish.map((fin)=>{return fin.GID});
    const p_finother = p_data.filter((tt)=>{return p_fin.indexOf(tt.GID) < 0});
    commit('setVipActivityFinish', state.memberData.activityInfo.finish.concat(p_finother))

    return new Promise(resolve => resolve(p_data))
  },
  //活動簽到資料
  async fetchVipActivityLog({getters, commit},Info) {
    // 必須為登入狀態
    commit('setVipActivityFinish', [])
    if (!getters.isLogin || !Info.GID) return
    const url = state.api.memberUrl
    const body = getters.appBodyWuhu({act: 'getactivitylog',ActivityID:Info.GID,"ActivityDays":31})
    const data = await fetchData({url, body})
		// IsHank && alert('活動簽到資料-body: \n'+JSON.stringify(body));	// @@

    if (data.error) return
    commit('setVipActivityFinish', data)
  },
  //活動簽到得獎紀錄
  async fetchVipAwards({getters, commit}) {
    // 必須為登入狀態
    if (!getters.isLogin) return
    const url = state.api.memberUrl
    const body = getters.appBodyWuhu({act: 'getawards'})
    const data = await fetchData({url, body})
    if (data.error) return
    commit('setVipAwards', data)
    return new Promise(resolve => resolve(data))
  },
  // (卡卡) 取 currentAppUser 的相關資料
  async fetchAppUserData({dispatch, getters, commit, state}){
    // 必須為登入狀態
    if (!getters.isLogin) return

		const url = state.api.memberUrl
    const body = getters.appBody({act: 'GetVipAndCardInfo'})
    const data = await fetchData({url, body})
    if (data.error) return

    return new Promise(resolve => {
      commit('setMemberVipData', data)
      // 取會員的消費紀錄
      dispatch('fetchMemberPurchaseRecordData')
      // 取會員的積點交易紀錄
      dispatch('fetchMemberExpiredPointsData')
      // 取優惠券列表資訊
      dispatch('fetchMemberTicketListData')
      // 取會員已轉贈的優惠券資訊
      dispatch('fetchMemberGaveTicketListData')
      // 取尚未能使用的優惠券資料
      dispatch('fetchMemberNotAvailableTickets')

      resolve(true)
    })
  },
  // (卡卡) 取 currentAppUser 的相關資料 - 五戶
  async fetchAppUserDataWuhu({getters, commit, state}){
    // 必須為登入狀態
    if (!getters.isLogin) return

    const url = state.api.picUrl + '/public/AppVIP.ashx'
    const body = getters.appBodyWuhu({act: 'GetVipAndCardInfo'})
    const data = await fetchData({url, body})
    if (data.error) return

    return new Promise(resolve => {
      commit('setMemberVipData', data)
      // // 取會員的消費紀錄
      // dispatch('fetchMemberPurchaseRecordData')
      // // 取會員的積點交易紀錄
      // dispatch('fetchMemberExpiredPointsData')
      // // 取優惠券列表資訊
      // dispatch('fetchMemberTicketListData')
      // // 取會員已轉贈的優惠券資訊
      // dispatch('fetchMemberGaveTicketListData')
      // // 取尚未能使用的優惠券資料
      // dispatch('fetchMemberNotAvailableTickets')

      resolve(true)
    })
  },
  // (卡卡) 取 cardQRcode
  async fetchCardQRCodeData({state, commit, getters}, GID) {
    // 先清空舊的 QRcode
    commit('setCardQRCodeData', { value: '', image: '', loaded: false } )
    const url = state.api.memberUrl
    const body = getters.appBody({act: 'GetVipCardQrCodeStr', GID})
    const data = await fetchData({url, body})
    if (data.error) return
    commit('setCardQRCodeData', { value: data.QrCode, image: data.QrCodeImg, loaded: true } )

  },
  // (龍海) 取 cardQRcode
  async fetchWuhuCardQRCodeData({state, commit, getters}, GID) {
    // 先清空舊的 QRcode
    commit('setCardQRCodeData', { value: '', image: '', loaded: false } )

    const url = state.api.memberUrl
    const body = getters.appBodyWuhu({act: 'GetVipCardQrCodeStr', GID})
    const data = await fetchData({url, body})
    if (data.error) return

    commit('setCardQRCodeData', { value: data.QrCode, image: data.QrCodeImg, loaded: true } )

  },
  // (全餐) 取領券QRcode
  async fetchDrawTicketQRCode({state, getters}, GID) {

    const url = state.api.memberUrl
    const body = getters.appBodyWuhu({act: 'GetDrawTicketQrCodeStr', GID})
    const data = await fetchData({url, body})
    if (data.error) return data.detail

    return data
  },
  // 取首頁所需資料
  async fetchData4CardIndex({dispatch}){
    // console.log('fetchData4CardIndex ===> start')
    dispatch('fetchIndexSlideBannersWuhu')
    dispatch('fetchIndexSubBannerWuhu')
    // 自動領券(要在fetchVipAndCardInfoWuhu之前且要await)
    await dispatch('fetchGetDrawAutoTicketWuhu')
    dispatch('fetchVipAndCardInfoWuhu') // for 點數 + QR Code
  },

  // (公開) 取 currentApp 的公開資料 - HomeCoupon頁
  async fetchAppPublicDataCouponWuhu({dispatch}){
    // 自動領券(要在fetchMemberTicketListDataWuhu之前且要await)
    //console.log("xxxx>>1/cardCoupon我的優惠券");
    await dispatch('fetchGetDrawAutoTicketWuhu')

    // 取優惠券列表資訊
    dispatch('fetchMemberTicketListDataWuhu')	// 抓-可使用的
    // 取會員已轉贈的優惠券資訊
    dispatch('fetchMemberGaveTicketListDataWuhu')	// 抓-已轉贈的
    // 取尚未能使用的優惠券資料
    dispatch('fetchMemberNotAvailableTicketsWuhu')	// 抓-未生效的
  },

  // (公開) 取 currentApp 的公開資料 - card/Member頁
  async fetchAppPublicDataDetailMemberWuhu({dispatch}){
    // 自動領券(要在fetchVipAndCardInfoWuhu之前且要await)
    await dispatch('fetchGetDrawAutoTicketWuhu')

    dispatch('fetchVipAndCardInfoWuhu') // for 點數 + QR Code
    // 取優惠券列表資訊
    dispatch('fetchMemberTicketListDataWuhu')
  },

  // (公開) 取 currentApp 的公開資料 - card/Point
  async fetchAppPublicDataDetailPointWuhu({dispatch}){
    // 取點數等資料
    dispatch('fetchVipAndCardInfoWuhu')
    // 取會員的積點交易紀錄
    dispatch('fetchMemberExpiredPointsDataWuhu')
  },

  // (公開) 取 currentApp 的公開資料 - card/Prepay
  async fetchAppPublicDataDetailPrepayWuhu({dispatch}){
    dispatch('fetchVipAndCardInfoWuhu') // for 點數
    dispatch('fetchMemberDepositRecordDataWuhu') // for 儲值紀錄
  },

  /** @Maybe: 未來可能不需要,待刪 */
  // (公開) 取 currentApp 的公開資料
  // async fetchAppPublicData({dispatch}){
    // // 取企業 app 的 connect
    // await dispatch('fetchAppConnect')
    // // 取企業 app 的 LOGO
    // dispatch('fetchAppLogo')
    // // 取首頁主 banners
    // dispatch('fetchIndexSlideBannersWuhu')
    // // 取首頁副 banners
    // dispatch('fetchIndexSubBannerWuhu')
    // // 取最新消息的資料
    // dispatch('fetchNewsData')
    // // 取最新消息的分類
    // dispatch('fetchNewsTypes')
    // // 取品牌資訊的資料
    // // dispatch('fetchBrandInfo')
    // // 取門店資訊的資料
    // // dispatch('fetchStoreListData')
    // dispatch('fetchStoreListDataWuhu')
    // // 取 menu 資料
    // dispatch('fetchMenuData')
  // },
  async fetchAppBrandData({dispatch}, payload){
    await dispatch('fetchCompanyData')
    dispatch('fetchBrandLink')
    await dispatch('fetchBrandNews')
    dispatch('fetchCloudsearch')
    if (payload && payload.gid) dispatch('fetchBrandData', {GID: payload.gid})

  },
  async fetchAppBrandDataById({dispatch, state}, payload){
    await dispatch('fetchCompanyData')
    let id = payload ? payload.gid : null
    let curCom = state.baseInfo.publicData.companyData.find((item)=>{
      return item.ID == id
    })
    let gid = curCom ? curCom.GID : null
		// console.log('fetchAppBrand1ById-payload,gid: ', payload, gid);	// @@

    dispatch('fetchBrandLink')
    await dispatch('fetchBrandNews')
    // dispatch('fetchCloudsearch')
    gid && dispatch('fetchBrandData', {GID: gid})

  },
  async fetchBrandDataGID({dispatch}, payload){
    payload.gid && dispatch('fetchBrandData', {GID: payload.gid})
  },
  // (公開) 取企業 App 的 db connect 資料
  async fetchAppConnect({state, commit}){
    const url = state.api.memberUrl
    const body = {"act":"db_name","EnterPriseID": state.currentAppUser.EnterPriseID}
    const connect = await fetchData({url, body})
    if (connect.error) return

    commit('setAppConnect', connect)
  },
  // (公開) 取企業 App 的 LOGO 資料
  async fetchAppLogo({state, getters, commit}){
    const url = state.api.memberUrl
    const body = {act: "getapplogo", EnterPriseID: getters.currentApp.EnterpriseID}
    const data = await fetchData({url, body})
    if (data.error) return

    const logoUrl = getters.srcUrl + data[0].Dir + data[0].FileName

    commit('setAppLogo', logoUrl)
  },

  // [五戶](公開) 取企業 App 的 LOGO 資料
  async fetchAppLogoWuhu({state, commit}){
    const url = state.api.memberUrl
    const body = {act: "getapplogo", EnterPriseID: state.baseInfo.EnterpriseID}
    const data = await fetchData({url, body})

    if (data.error) return

    const logoUrl = state.api.picUrl + data[0].Dir + data[0].FileName

    commit('setAppLogoWuhu', logoUrl)
  },

  // [五戶]撈[我的預約]清單
  async fetchMyBookList({getters, commit}){
    const url = getters.appBookUrlWuhu
    const body = getters.appBodyWuhu({act: 'get_mybooking'})
    const data = await fetchData({url, body})

    if (data.error) return

    commit('setMyBookList', data.mybooking)
    commit('setMyBookStatusList', data.status)
  },
  // [五戶]撈[商城]大小類等
  async fetchMallData({state}){
    const reloadShopFn = state.mallData.reloadShopFn;
    IsFn(reloadShopFn) && reloadShopFn();
  },
  // [五戶]撈[商城]記錄
  async fetchShopMine({state}){
    const reloadShopMineFn = state.mallData.reloadShopMineFn;
    IsFn(reloadShopMineFn) && reloadShopMineFn();
  },
  // [五戶]撈[商城]小類下品項,或品項的規格
  async fetchMallKindData({state},query){
    let isMallKindLoad = (query.goto!=undefined)//商城只允許第一個小類可刷新
    let isMallProductLoad = (query.type!=undefined && query.type === 'product');

    if (isMallProductLoad && IsFn(state.mallData.reloadProductFn)){
      state.mallData.reloadProductFn();

    }else if (isMallKindLoad && IsFn(state.mallData.reloadGoKindFn)) {
      state.mallData.reloadGoKindFn();
    }
  },
	/** @Remember: 廣告(AD)檔案做在get_advertgroup.json,並非GetBrandBanner */
  // (公開) 取廣告業資料
  async fetchAdvertise({commit}){
    const _act = 'get_advertgroup'
		let data = await GetJsonData1(_act)
		/** @@Test 彈跳廣告 */
		// data = await GetJsonTest(_act)	// @@
		data.ADBanner = FilterOnSale.call(data.ADBanner, 'No');
		// console.log('取廣告業資料_data: ', data);	// @@

    commit('setAdvertise', data)
  },
  // (公開) 取公司資料: 隱私權政策,關於我們...
  async fetchCompanyData({commit}){
		let data = await GetJsonData1('get_cloudbrand');

		/** @@Test 副Banner資料 */
		// data = await GetJsonTest('get_cloudbrand')	// @@

		data = FilterOnSale.call(data, 'ID');
		commit('setCompanyData', data)
	},
	// (公開) 取公司資料: 雲商品
	async fetchBrandData({state, getters, commit}, payload){
		const _act1 = 'get_brandproduct'
		let data		= S_Obj.read(_act1) || []
		if (!data.length) {
			const url = state.api.figUrl
				,body 	= getters.appBodyWuhu({act:_act1,GID:payload.GID})
			data = await fetchData({url, body})

			S_Obj.save(_act1, data);
		}
		/** @@Test 雲商品 */
		// data = await GetJsonTest('get_brandproduct')	// @@
		// IsDev && console.log('取雲商品-fetchBrandData: ', data);	// @@

    if (data.error) return commit('setBrandData', [])
    commit('setBrandData', data)
  },
	// (公開) 取公司資料: 雲商品,取 KindID
	async fetchBrandDataKind({state, getters, commit}, payload){
    const url = state.api.figUrl
    const body = getters.appBodyWuhu({act:"get_brandproduct",KindID:payload.KindID})
    let data = await fetchData({url, body})
    if (data.error) return commit('setBrandDataKind', [])
    commit('setBrandDataKind', data)
  },
	// (公開) 取公司資料: 雲商品,取 DetailID
	async fetchBrandDataFood({state, getters, commit}, payload){
    const url = state.api.figUrl
    const body = getters.appBodyWuhu({act:"get_brandproduct",DetailID:payload.DetailID})
    let data = await fetchData({url, body})
    if (data.error) return commit('setBrandDataDetail', [])
    commit('setBrandDataDetail', data)
  },
  //品牌下的社群
  async fetchBrandLink({state, getters, commit}){
		const _act = 'get_brandlink'
		let data = await GetJsonData1(_act)
		if (!data.length) {	// 打api
			const url = state.api.figUrl
			const body = getters.appBodyWuhu({act:_act})
			data = await fetchData({url, body})
			if (data.error) return
		}
    commit('setBrandLink', data)
  },
  //品牌下的消息
  async fetchBrandNews({state, getters, commit}){
		const _act = 'get_brandmsg'
		let data = await GetJsonData1(_act)
		if (!data.length) {	// 打api
			const url = state.api.figUrl
			const body = getters.appBodyWuhu({act:_act})
			data = await fetchData({url, body})
			if (data.error) return
		}
		/** @@Test 最新消息 */
		// data = await GetJsonTest(_act)	// @@
		data = FilterOnSale.call(data, 'No');
		// console.log('fetchBrandNews-get_brandmsg: ', data);	// @@

    commit('setBrandNews', data)
  },
  //品牌下的某一筆消息
  async fetchBrandNewsById({state, getters, commit}, payload){
		const _act 	= 'get_brandmsg'
			,_id			= payload
		let data = await GetJsonData1(_act)
		if (!data.length) {	// 打api
			const url = state.api.figUrl
			const body = getters.appBodyWuhu({act:_act, "DetailID":_id})
			data = await fetchData({url, body})
			if (data.error) return
		}

		/** @@Test 最新消息 */
		// data = await GetJsonTest(_act)	// @@
		// console.log('fetchBrandNewsById-get_brandmsg: ', data);	// @@

		let arr1 = data.filter((item) => {return item.DetailID == _id});
		// console.log('GetJSON_BrandNews_id,arr1: '+_id, arr1); // @@
    commit('setBrandNewsOne', arr1)
  },
   //搜尋列設定
   async fetchCloudsearch({state, getters, commit}){
    const url = state.api.figUrl
    const body = getters.appBodyWuhu({act:"get_cloudsearch"})
    let data = await fetchData({url, body})
    if (data.error) return
    commit('setCloudsearch', data)
  },
  // (公開) 取主畫面可滑動的 Banners 資料
  async fetchIndexSlideBannersWuhu({commit}, payload){
		let data = await GetJSON_Banner(payload, 'M')
    if (data.error) return
    commit('setIndexSlideBannersWuhu', data)
  },

  // (公開) 取固定的副 Banner 資料
  async fetchIndexSubBannerWuhu({commit}, payload){
		let data = await GetJSON_Banner(payload, 'D')

    if (data.error) return
		/** @@Test 副Banner資料 */
		// let data = await GetJsonTest('subBanner')	// @@
    commit('setIndexSubBannerWuhu', data)
  },
  // (公開) 取最新消息的資料
  async fetchNewsData({state, commit, getters}){
    const url = state.api.publicUrl
    const body = getters.appBody({act:"GetBrendNews"})
    const data = await fetchData({url, body})
    if (data.error) return

    commit('setNewsData', data)
  },
  // (公開) 取最新消息的資料 - 五互
  async fetchNewsDataWuhu({state, commit, getters}){
    const url = state.api.publicUrl
    const body = getters.appBodyWuhu({act:"GetBrendNews"})
    const data = await fetchData({url, body})
    if (data.error) return

    commit('setNewsDataWuhu', data)
  },
  // (公開) 取最新消息的分類
  async fetchNewsTypes({state, commit, getters}){
    const url = state.api.publicUrl
    const body = getters.appBody({act:"GetBrendNewsType"})
    const data = await fetchData({url, body})
    if (data.error) return

    commit('setNewsTypes', data)
  },
  // (公開) 取品牌資訊的資料	// hank:應該是舊資料,待拿掉...
  // async fetchBrandInfo({state, commit, getters}){
    // const url = state.api.kakarUrl
    // const body = { act: "getbrandmsg", EnterpriseID: getters.currentApp.EnterpriseID, brandid: "1,2,3,4,5,6,7,8,9" }
    // const data = await fetchData({url, body})
    // if (data.error) return

    // commit('setBrandInfo', data)
  // },

  // (公開) 取門店資訊的資料 - 五互
  async fetchStoreListDataWuhu({state, getters, commit}){
		const _act1 = 'GetVipMainEnterpriseID'
			,_act2 		= 'GetStoreList2'
			,_act3 		= 'GetAishileAppMode'
			,EPID 		= state.baseInfo.EnterpriseID
			// ,memberUrl 	= `https://${baseUrl}/Public/AppVipOneEntry.ashx`
			,memberUrl	= 'https://8012.jh8.tw/Public/AppVipOneEntry.ashx'

		let dataA			= S_Obj.read(_act1)
			,stores			= S_Obj.read(_act2)
			,storeMode	= S_Obj.read(_act3)
			,MainEnterPriseID, MainConnect
		// console.log('S_Obj取得資料: ', dataA, stores, storeMode);	// @@

		// 取得主企業號 ID & connect
		if (!dataA) {
			const bodyA = {act:_act1, EnterpriseID: EPID}
			dataA = await fetchData({url: memberUrl, body: bodyA})
			S_Obj.save(_act1, dataA);
			IsDev && console.log('取得主企業號-GetVipMainEnterpriseID: ', dataA);	// @@
		}

		MainEnterPriseID = dataA.EnterpriseID
		MainConnect = dataA.connect

		// 取得門店資料
		if (!stores) {
			const urlB 	= state.api.publicUrl
				,bodyB	= getters.appBodyWuhu({act:_act2})
			stores = await fetchData({url: urlB, body: bodyB})
			S_Obj.save(_act2, stores);
			IsDev && console.log('取得門店資料-GetStoreList2: ', stores);	// @@
		}

		//取得門店的線上點餐參數
		if (!storeMode) {
			const bodyC = {act:_act3, EnterpriseID: EPID, FunctionID: "310703"}
			// 將線上點餐的參數(.Mode)存入到門店資料裡面
			storeMode = await fetchData({url: memberUrl, body: bodyC})
			storeMode.length && S_Obj.save(_act3, storeMode);
			IsDev && console.log('線上點餐的參數-storeMode: ', storeMode);	// @@
		}

    state.baseInfo.publicData.storeIsLoaded = true
    // 存入到 Vuex 裡面
    commit('setStoreListDataWuhu', {stores, MainEnterPriseID, MainConnect, storeMode})
  },

  // 取得門店頁需要的資料
  fetchStoreData({dispatch}){
    dispatch('fetchStoreListDataWuhu')
    dispatch('fetchCloudsearch')
  },

  // (公開) 取 Menu 資料
  async fetchMenuData({state, getters, dispatch}) {
    const url = state.api.publicUrl
    const body = getters.appBody({act:"GetBrandProductType"})
    const menuKindIds = await fetchData({url, body})
    if (menuKindIds.error) return
    menuKindIds.forEach(item => dispatch('fetchMenuMainTypeData', {code: item.KindID, name: ''}) )
  },
  // (公開) 取 Menu 大類的資料
  async fetchMenuMainTypeData({state, commit, getters}, payload) {
    // 如果已經有資料則離開
    const dataIsOwned = state.publicData.menu.mainType.find(item => { return item.code === payload.code }) !== undefined
    if (dataIsOwned) return

    const url = state.api.publicUrl
    const body = getters.appBody({act:"GetBrandProductType", MKindID: payload.code})
    const data = await fetchData({url, body})
    if (data.error) return

    commit('setMenuMainTypeCodeName', payload)
    commit('setMenuMainTypeListData', { code: payload.code, data: data })
  },

  // (公開) 取某個雲商品的資料 (用DetailID)
  async fetchProductData({state, getters, commit}, payload){
    // const url = state.api.figUrl
		const isTrue1		= S_Obj.isStaging || S_Obj.isDebug
			,url 					= isTrue1 ? state.api.Url8012 : state.api.figUrl
			,body = getters.appBodyWuhu({act:"get_brandproduct",DetailID:payload})
			,data = await fetchData({url, body})
		// console.log('取雲商品資料-id,data: '+payload, data);	// @@

    if (data.error) return commit('setCurrentProduct', [])
    commit('setCurrentProduct', data)
  },

  // (公開) 取 Menu 小類的資料
  async fetchMenuSubTypeData({state, commit, getters}, payload) {
    // 如果已經有資料則離開
    const dataIsOwned = state.publicData.menu.subType.find(item => item.code === payload.code ) !== undefined
    if (dataIsOwned) return

    const url = state.api.publicUrl
    const body = getters.appBody({act:"GetBrandProduct", KindID: payload.code})
    const data = await fetchData({url, body})
    if (data.error) return

    commit('setMenuSubTypeCodeName', payload)
    commit('setMenuSubTypeListData', { code: payload.code, data: data } )
  },
  fetchFoodmarketMulti({state,dispatch}){
    // console.log("fetchFoodmarketMulti",state.mallData.foodkind);
    //迴圈一起跑會GG (API無法負荷??),已改為1筆1筆取,第一筆回應,再next
    if (Array.isArray(state.mallData.foodkind) && state.mallData.foodkind.length > 0) dispatch('fetchFoodmarketData',{kindID:state.mallData.foodkind[0]["ID"],kIndex:0})
  },
  // 取全部小類下商品
  async fetchFoodmarketData({commit,getters,dispatch},p_obj) {
    const url =  getters.appBookUrlWuhu;
    // console.log("fetchFoodmarketData",url,p_obj.kindID,p_obj.kIndex);
    const body = getters.appBodyWuhu({"act": 'get_foodmarket',"FoodKind": p_obj.kindID});
    const data = await fetchData({url, body});
    var isErr = data.detail && data.detail.ErrorCode && data.detail.ErrorCode != "0";

    if (isErr || data.error) return
    commit('setFoodmarketData', {foods: (data.FoodMarket || []), kindID: p_obj.kindID})
    const p_index = p_obj.kIndex+1;
    //已改為1筆1筆取,等待第一筆回應,再next
    if (state.mallData.foodkind.length > p_index) dispatch('fetchFoodmarketData',{kindID:state.mallData.foodkind[p_index]["ID"],kIndex:p_index})

  },
  /* 企業會員資料相關 */
  // 取會員的消費紀錄
  async fetchMemberPurchaseRecordData({commit, getters}) {
    const url = state.api.memberUrl
			,body 	= getters.appBodyWuhu({"act":"GetVipPosItems"})
			,data 	= await fetchData({url, body})
    if (data.error) return

		/** @@Test 購買交易紀錄 */
		// let data = await GetJsonTest('purchaseRecord')	// @@
    commit('setMemberPurchaseRecordData', data)
  },
  // 取會員的積點交易紀錄
  async fetchMemberExpiredPointsData({state, commit, getters}) {
    const url = state.api.memberUrl
    const body = getters.appBody({"act":"GetVipRecord", "TradeTypeCode":"4,-4,6,-6,7,-7,15,-15,16,-16,17,-17,71,-71,72,-72"})
    const data = await fetchData({url, body})
    if (data.error) return

    commit('setMemberExpiredPointsData', data)
  },
  // 取會員的積點交易紀錄 - 五互
  async fetchMemberExpiredPointsDataWuhu({state, commit, getters}) {
    const url = state.api.memberUrl
    // 1      : 激活
    // 4,   -4: 積分兌換, 積分兌換退回
    // 6,   -6: 結帳消費, 結帳消費退回
    // 7,   -7: 積分換券, 積分換券退回
    // 13     : 卡片升級              (沒有-13)
    // 15, -15: 公關贈點, 公關贈點退回
    // 16, -16: 活動贈點, 活動贈點退回
    // 17, -17: 商品贈點, 商品贈點退回
    // 24, -24: 綁定禮, 	綁定禮退回
    // 71, -71: 點數轉贈, 點數轉贈退回
    // 72, -72: 點數受贈, 點數受贈退回
    // 73, -73: 簽到禮, 	簽到禮退回
    // 82: 卡片滙入
    const body = getters.appBodyWuhu({"act":"GetVipRecord", "TradeTypeCode":"1,4,-4,6,-6,7,-7,13,15,-15,16,-16,17,-17,24,-24,31,-31,71,-71,72,-72,73,-73,82"})
    const data = await fetchData({url, body})
		// console.log('取會員的積點交易紀錄-data: ', data);	// @@
    if (data.error) return
    state.baseInfo.memberData.expiredPointsIsLoaded = true

    commit('setMemberExpiredPointsDataWuhu', data)
  },
  // 取會員的儲值交易紀錄
  async fetchMemberDepositRecordDataWuhu({state, commit, getters}) {
    const url = state.api.memberUrl
    const body = getters.appBodyWuhu({"act":"GetVipRecord", "TradeTypeCode":"3,-3,2,-2,19"})
    const data = await fetchData({url, body})
    if (data.error) return
    state.baseInfo.memberData.depositRecordIsLoaded = true

		/** @@Test 可領用的優惠券列表 */
		// let data = await GetJsonTest('depositRecord')	// @@
    commit('setMemberDepositRecordData', data)
  },
  // 取會員的[消費點數紀錄]=[契約] - 五互
  // 秋林版 - 目前已改用黃大哥版(如下)
  async fetchMemberConsumerPointsDataWuhu_bak({state, commit, getters}) {
    const url = state.api.memberUrl
    const body = getters.appBodyWuhuEncode({
      "act":"WUHU_Deed_Points_Info_List_all",
      "FunctionID":"950201",
      "orderby":"end_day desc, deed_no desc",
      "srow": 0 // srow=0代表傳全部
    })
    const data = await fetchData({url, body})
    if (data.error) return

    commit('setMemberConsumerPointsDataWuhu', data)
  },
  // 取會員的[消費點數紀錄]=[契約] - 五互
  async fetchMemberConsumerPointsDataWuhu({/*state,*/ commit, getters}, payload) {
    const url = getters.appBookUrlWuhu

    const dynamicBody = (payload || {})
    let fixData = {
      "act":"get_deedpoints",
      "orderby":"end_day desc, deed_no desc"
    }
    let allData = Object.assign({}, fixData, dynamicBody)

    const body = getters.appBodyWuhu(allData)
    const data = await fetchData({url, body})
    if (data.error) return

    commit('setMemberConsumerPointsDataWuhu', data)
  },
  // 取會員的單一筆[契約][消費點數紀錄]=[契約] - 五互
  async fetchMemberConsumerPointsDetailWuhu({state, commit, getters}, payload) {
    const url = state.api.memberUrl
    const body = getters.appBodyWuhuEncode({
      "act":"WUHU_Deed_PointsBalance_Trade",
      "FunctionID":"950201",
      "orderby":"LastModify",
      "srow": 0 // srow=0代表傳全部
    }, {
      "deed_no": btoa(payload.deed_no)
    })
    const data = await fetchData({url, body})
    if (data.error) return

    commit('setMemberConsumerPointDetailWuhu', data)
  },
  // 取會員的單一筆[訂單][消費點數紀錄] - 用Order No
  async fetchOneOrderByOrderNo({/*state,*/ commit, getters}, payload) {
    const url = getters.appBookUrlWuhu
    if (payload) {
      const dynamicBody = (payload.requestBody || {})
      let fixData = {
        "act":"get_orderitems"
      }
      let allData = Object.assign({}, fixData, dynamicBody)

      const body = getters.appBodyWuhu(allData)
      const data = await fetchData({url, body})
      if (data.error) return

      let orderDetail = data?data.OrderItem:null

      commit('setOneOrderDetail', {
        orderDetail: orderDetail,
        arrayIndex: payload.arrayIndex
      })
    }
  },
  /* 取站別,店家名稱(點數折抵-龍海專用) */
  async GetCustomerCode({state, getters}, codeID) {
		// const url = state.api.memberUrl
		const isTrue1	= S_Obj.isStaging || S_Obj.isDebug
			,url 				= isTrue1 ? state.api.Url8012 : state.api.memberUrl
		// alert('GetCustomerCode-codeID: '+codeID);	// @@

    const body = getters.appBodyWuhu({
      act: "getcustomercode",
      Account: state.member.code,
      CustomerCode: codeID,
    })
    const data 	= await fetchData({url, body})
			,hasOK 		= !data.error
    return new Promise(resolve => {
			let res1 = false
			if (hasOK && data.length) {
				res1 = data[0];

			} else {
				showAlert(data.msg);
			}
      resolve(res1)
    })
  },
  /* 點數折抵(龍海專用) */
	async VipAmountToPoint({state, getters}, payload) {
		// const url = state.api.memberUrl
		const isTrue1	= S_Obj.isStaging || S_Obj.isDebug
			,url 			= isTrue1 ? state.api.Url8012 : state.api.memberUrl
			,account	= state.member.code
			,head 		= { "headers": { "Content-Type": "application/x-www-form-urlencoded" } }
			// ,head 		= getters.requestHead		// NG!(跨域
			,body 		= getters.appBodyWuhu({
      act: "custvipamounttopoint",
      ShopID: payload.ShopID,
      Account: account,
			Point: {
				TradeNum: GUID(),
				CardId: account,
				TradeAmount: payload.Point,
				OrderID: "",
				OrderNO: "",
				CheckID: "",
			},
    })

    // const obj1 = await fetchData({url, body})	// nono!(格式不符
    const res1 	= await axios.post( url, body, head )
			,obj1 		= res1.data
    return new Promise(resolve => resolve(obj1))
  },

  async ensureGetDeviseGPS({/*state,*/ commit}) {
    if (IsMobile()) {
      // (在殼裡面) call 跟殼取 GPS 資訊的 API 與 callback function
      deviseFunction('gps', '', '"cbFnSyncDeviseGPS"')
    } else {
      // 不在殼裡面
      if ("geolocation" in navigator) {
        /* geolocation is available */
        navigator.geolocation.getCurrentPosition((position)=>{
          //console.log('navigator ===> position =>', position)
          // 轉成 google maps api 需要的預設格式
          const gpsData = {gps: {
            lat: position.coords.latitude,
            lng: position.coords.longitude} }
          // 將 gps 存入 state.baseInfo
          commit('setBaseInfo', gpsData)
        }, ()=>{
          console.log('navigator ===> error => no navigator')
        })
      } else {
        /* geolocation IS NOT available */
      }
    }
  },
  /* 取可領用的優惠券列表資訊 + 領取優惠券 - 五互 */
  async fetchMemberDrawTicketListDataWuhu({state, commit, getters}){
    const url = state.api.memberUrl
    const body = getters.appBodyWuhu({ act: "GetDrawTicketList" })
    let data = await fetchData({url, body})
    if (data.error) return

		/** @@Test 可領用的優惠券列表 */
		// data = await GetJsonTest('drawTicketList')	// @@
    commit('setMemberDrawTicketListData', data)
  },
  /* 自動領券(單純告訴後端,要開始領了的觸發act... */
  async fetchGetDrawAutoTicketWuhu({state, getters}){
		if (IsRunAutoTicketed) return new Promise(resolve => resolve(true))
		if (!getters.isLogin)		return

    const url = state.api.memberUrl
			,body 	=	getters.appBodyWuhu({ act: "getdrawautoticket" })
			,data 	= await fetchData({url, body})
			,hasOK	= !data.error
		return new Promise((resolve) => {
			/** @Important: 秋哥說: 啟動後只要發過1次即可,不需再發了 */
			hasOK && (IsRunAutoTicketed = true);
			resolve(hasOK);
		})
  },
  /* 取優惠券列表資訊 */
  async fetchMemberTicketListData({state, commit, getters}){
    const url = state.api.memberUrl
			,body 	=	getters.appBody({ act: "GetVipTicket" })
			,data 	= await fetchData({url, body})
    if (data.error) return
    commit('setMemberTicketListData', data)
  },
  /* 取優惠券列表資訊 - 五互,fig用此 */
  async fetchMemberTicketListDataWuhu({state, commit, getters}){
    if (!getters.isLogin)		return
    const url = state.api.memberUrl
			,body 	=	getters.appBodyWuhu({ act: "GetVipTicket" })
			,data 	= await fetchData({url, body})
    if (data.error) return

		/** @@Test 優惠券列表資訊 */
		// let data = await GetJsonTest('ticketList')	// @@
    commit('setMemberTicketListDataWuhu', data)
  },
  // 取已贈送的優惠券列表資訊
  async fetchMemberGaveTicketListData({state, commit, getters}){
    const url = state.api.memberUrl
			,body 	=	getters.appBody({ act: "GetGaveTicketRecord" })
			,data 	= await fetchData({url, body})
    if (data.error) return

    commit('setMemberGaveTicketListData', data)
  },
  // 取已贈送的優惠券列表資訊 - 五互
  async fetchMemberGaveTicketListDataWuhu({state, commit, getters}){
    const url = state.api.memberUrl
			,body 	=	 getters.appBodyWuhu({ act: "GetGaveTicketRecord" })
			,data 	=  await fetchData({url, body})
    if (data.error) return

		/** @@Test 已贈送的優惠券列表資訊 */
		// let data = await GetJsonTest('gaveTicketList')	// @@
    commit('setMemberGaveTicketListDataWuhu', data)
  },
  // 取尚未能使用的優惠券資料
  async fetchMemberNotAvailableTickets({state, commit, getters}){
    const url = state.api.memberUrl
			,body 	=	getters.appBody({ act: "NotTodayTicketList" })
			,data 	= await fetchData({url, body})
    if (data.error) return

    commit('setMemberNotAvailableTickets', data)
  },
  // 取尚未能使用的優惠券資料 - 五互
  async fetchMemberNotAvailableTicketsWuhu({state, commit, getters}){
    const url = state.api.memberUrl
			,body 	=	getters.appBodyWuhu({ act: "NotTodayTicketList" })
			,data 	= await fetchData({url, body})
    if (data.error) return

		/** @@Test 可領用的優惠券列表 */
		// let data = await GetJsonTest('notAvailableTickets')	// @@
    commit('setMemberNotAvailableTicketsWuhu', data)
  },
  // 優惠券贈送
  async ensureMemberTicketTrans({state, getters}, payload){
    const url = state.api.memberUrl
			,body 	=	getters.appBodyWuhu(Object.assign({}, { act: "SetTicketToOther" }, payload ))
			,data 	= await fetchData({url, body})
    if (data.error) return data.detail

    return data
  },
  // 點數贈送,轉贈-點數
  async ensureMemberTransPoint({state, getters}, payload){
    const url = state.api.memberUrl
			,body 	=	getters.appBodyWuhu(Object.assign({}, { act: "SetPointtoOther" }, payload ))
			,data 	= await fetchData({url, body})
    if (data.error) return data.detail

    return data
  },
  // 取優惠券明細的使用條款與兌換條件
  fetchMemberTicketInfoData({dispatch}, ticket){
    // 取該卷使用的 QRcode
    dispatch('fetchTicketQRCodeData', ticket.GID)
    // 取該卷的條款與規則
    dispatch('fetchTicketTradeRules', ticket.TicketTypeCode)
  },
  // 取優惠券條碼 QRcode
  async fetchTicketQRCodeData({state, commit, getters}, ticketGID){
    // 先清空現在的 QRcode 資料
    commit('setTicketQRCodeData', {gid: '', image: ''} )
    setLoading(true)
    const url = state.api.memberUrl
			,body 	=	getters.appBodyWuhu({ act: "GetTicketQrCodeStr", GID: ticketGID, QRCodeScale: 4 })
			,data 	= await fetchData({url, body})
    setLoading(false)
    if (data.error) return

    commit('setTicketQRCodeData', {gid: data.QrCode, image: data.QrCodeImg} )
  },
  // 取優惠券明細資訊: 規則條款
  async fetchTicketTradeRules({state, commit, getters, dispatch}, TypeCode){
    setLoading(true)
    const url = state.api.memberUrl
    const body = getters.appBodyWuhu({ act: "GenerateTicketTradeRules", TicketTypeCode: TypeCode })
    let data = await fetchData({url, body})
    setLoading(false)
    if (data.error) return
    // console.log('取優惠券明細資訊_data: ', data);	// @@

		/** @@Test 優惠券明細資訊-規則條款 */
		// let data = await GetJsonTest('ruleData')	// @@

		const one = data.length ? data[0] : null;
		if (one) {
			// 取優惠券的兌換條件: 商品
			dispatch('fetchTicketUsageRuleFoods', one.TradeRuleCode)
			// 取優惠券的兌換條件: 門店
			dispatch('fetchTicketUsageRuleShops', one.TradeRuleCode)
			// 設定使用條款與適用日
			commit('setMemberTicketInfoData', { ruleData: one, currentTypeCode: TypeCode })
		}
  },
  // 取優惠券明細資訊的兌換條件: 商品
  async fetchTicketUsageRuleFoods({state, commit, getters}, RuleCode){
    // 已有相同資料就不再取
    if (state.memberData.ticketInfo.currentRuleCode === RuleCode) return
    setLoading(true)
    const url = state.api.memberUrl
    const body = getters.appBodyWuhu({ act: "GenerateTicketTradeRuleFoods", TradeRuleCode: RuleCode })
    let data = await fetchData({url, body})
    setLoading(false)
    if (data.error) return

		/** @@Test 優惠券明細資訊的兌換-商品 */
		// let data = await GetJsonTest('usageFoods')	// @@
    commit('setMemberTicketInfoData', { usageFoods: data, currentRuleCode: RuleCode } )
  },

  // 取優惠券明細資訊的兌換條件: 門店
  async fetchTicketUsageRuleShops({state, commit, getters}, RuleCode){
    // 已有相同資料就不再取
    if (state.memberData.ticketInfo.currentRuleCode === RuleCode) return
    setLoading(true)
    const url = state.api.memberUrl
    const body = getters.appBodyWuhu({ act: "GenerateTicketTradeRuleShops", TradeRuleCode: RuleCode })
    let data = await fetchData({url, body})
    // console.log('取優惠券兌換門店-data: ', data);	// @@
    setLoading(false)
    if (data.error) return

		/** @@Test 優惠券明細資訊的兌換-門店 */
		// let data = await GetJsonTest('usageShops')	// @@
    commit('setMemberTicketInfoData', { usageShops: data, currentRuleCode: RuleCode })
  },
  // 優惠券兌換門市
  async ensureTicketExchange({state, getters}, payload){
    const url = state.api.memberUrl
    const body = getters.appBodyWuhu(Object.assign({}, { act: "SetTicketExchange" }, payload ))
    const data = await fetchData({url, body})
		return !data.error ? data : data.detail;
  },
}


async function GetJSON_Banner(isBust, _type) {
	const arrObj = await GetJsonData1('GetBrandBanner')
	// console.log('GetJSON-_type: '+arrObj.length, _type); // @@

	switch(_type){
	case 'M':
		_type = "$M$"
		break;
	case 'D':
		_type = "$D$"
		break;
	default:	// AD
		_type = "AD"
	}

	let data = arrObj.length ? arrObj.filter((item) => {
		return item.BannerType == _type
	}) : []
	// console.log('GetJSON-arr1: ', data); // @@

	const isTrue = isBust || !data.length	// 有帶更新,或無值時...
	if (isTrue) {	// 打api
		const url = state.api.publicUrl
		const body = getters.appBodyWuhu({act:"GetBrandBanner", BannerType: _type})
		data = await fetchData({url, body})
	}
	// console.log('GetJSON_Banner-isBust: '+isBust, data);	// @@
	return FilterOnSale.call(data, 'No');
}

function FilterOnSale(_sort) {
	const today = formatTime(new Date(), 'YYYYMMDDHHmm')
	// console.log('FilterOnSale-today: '+_sort, today);	// @@

	if (Array.isArray(this) && this.length) {
		const them = this.filter((item) => {
			const _s1	= formatTime(item.SaleTime, 'YYYYMMDDHHmm')
				,_s2 		= formatTime(item.CloseTime, 'YYYYMMDDHHmm')
			// console.warn('Text,today: ', item.Text, today);	// @@
			// console.log('SaleTime, CloseTime: '+item.SaleTime, item.CloseTime);	// @@
			// const tt1 = item.SubText1 && item.SubText1.substr(0,16);	// @@
			// console.log('chNewsList-SubText1: ', tt1);	// @@
			return today >= _s1 && today <= _s2
		}).sort(function (a, b) {
			return parseInt(a[_sort]) > parseInt(b[_sort]) ? 1 : -1;
		})
		if (IsDev && !them.length) {	// 無東西的警示
			console.error('FilterOnSale-this,them: ', this, them);
		}

		return them;

	} else {
		return [];
	}
}

export default new Vuex.Store({ state, mutations, getters, actions })
